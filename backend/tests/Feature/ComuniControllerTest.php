<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace Tests\Feature;

use Tests\TestCase;

class ComuniControllerTest extends TestCase
{


    public function testList()
    {
        $response = $this->get('api/comuni/');

        $response->assertStatus(200);

        $json = $response->json()[0];
        $this->assertTrue(isset($json["nome"]));
        $this->assertTrue(isset($json["codiceCatastale"]));
    }


    public function testGetManfredonia()
    {
        $response = $this->get('api/comuni/E885');

        $response->assertStatus(200);

        $json = $response->json();

        $this->assertTrue(isset($json["nome"]));
        $this->assertTrue(isset($json["codiceCatastale"]));

        $this->assertEquals("E885", $json["codiceCatastale"]);
        $this->assertEquals("Manfredonia", $json["nome"]);
    }

    public function testGetComuneInesistente()
    {
        $response = $this->get('api/comuni/E88B');

        $response->assertStatus(404);

    }

    public function testVicini()
    {
        $response = $this->get('api/comuni/vicini/?codiceOri=E885');

        $response->assertStatus(200);
        $json = $response->json();
        $this->assertArrayHasKey("codiceCatastale", $json[0]);
        $this->assertArrayHasKey("nome", $json[0]);
        $this->assertArrayHasKey("minuti", $json[0]);


    }

    public function testViciniNoCod()
    {
        $response = $this->get('api/comuni/vicini/');

        $response->assertStatus(400);
    }

    public function testDistanza()
    {
        $response = $this->get('api/comuni/distanza/?codiceOri=E885&codiceDst=E885');

        $response->assertStatus(200);
        $json = $response->json();
        $this->assertArrayHasKey("codiceCatastaleOrigine", $json);
        $this->assertArrayHasKey("codiceCatastaleDestinazione", $json);
        $this->assertArrayHasKey("minuti", $json);


    }

    public function testDistanzaNoCod()
    {
        $response = $this->get('api/comuni/distanza/?codiceOri=E885&codiceDst=');
        $response->assertStatus(400);

        $response = $this->get('api/comuni/distanza/?codiceOri=&codiceDst=E884');
        $response->assertStatus(400);
    }

    public function testDistanzaNonTrovato()
    {
        $response = $this->get('api/comuni/distanza/?codiceOri=E885&codiceDst=E884');

        $response->assertStatus(400);
        $response->json();


    }


}
