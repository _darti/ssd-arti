<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace Tests\Unit;

use App\Models\AnnoScolastico;
use Tests\TestCase;

class AnnoScolasticoTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testAnnoScolastico()
    {
        $resultOk = AnnoScolastico::getAnnoScolastico("2018/2019");
        $resultFalse = AnnoScolastico::getAnnoScolastico("2018/20sdsd19");
        $this->assertEquals("2018/2019", $resultOk[0]);
        $this->assertEquals("2018", $resultOk[1]);
        var_dump($resultOk);
        $this->assertEquals("2019", $resultOk[2]);
        $this->assertFalse($resultFalse);

        $this->assertFalse(AnnoScolastico::getAnnoScolastico("2018"));
    }
}
