<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Repositories;


use App\Models\IstituzioneScolastica;
use App\Models\SedeCiclo2;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;


class ISRepository
{

    const numeroAlunniIsSovraDimensionata = 1200;
    const numeroAlunniIsSottoDimensionata = 600;
    const numeroAlunniIsSottoDimensionataComuneSvantaggiato = 400;
    const comuniSvantaggiati = [
        [
            "nome" => "Accadia",
            "codiceCatastale" => "A015"
        ],
        [
            "nome" => "Alberona",
            "codiceCatastale" => "A150"
        ],
        [
            "nome" => "Anzano Di Puglia",
            "codiceCatastale" => "A320"],
        [
            "nome" => "Cagnano Varano",
            "codiceCatastale" => "B357"
        ],
        [
            "nome" => "Carlantino",
            "codiceCatastale" => "B784"
        ],
        [
            "nome" => "Carpino",
            "codiceCatastale" => "B829"
        ],
        [
            "nome" => "Casalnuovo Monterotaro",
            "codiceCatastale" => "B904"
        ],
        [
            "nome" => "Castelluccio Valmaggiore",
            "codiceCatastale" => ""
        ],
        [
            "nome" => "Celenza Valfortore",
            "codiceCatastale" => "C202"
        ],
        [
            "nome" => "Celle Di San Vito",
            "codiceCatastale" => "C442"
        ],
        [
            "nome" => "Faeto",
            "codiceCatastale" => "D459"
        ],
        [
            "nome" => "Ischitella",
            "codiceCatastale" => "E332"
        ],
        [
            "nome" => "Mattinata",
            "codiceCatastale" => "F059"
        ],
        [
            "nome" => "Monte Sant'Angelo",
            "codiceCatastale" => "F631"
        ],
        [
            "nome" => "Monteleone Di Puglia",
            "codiceCatastale" => ""
        ],
        [
            "nome" => "Motta Montecorvino",
            "codiceCatastale" => "F481"
        ],
        [
            "nome" => "Panni",
            "codiceCatastale" => "G312"
        ],
        [
            "nome" => "Peschici",
            "codiceCatastale" => "G487"
        ],
        [
            "nome" => "Rocchetta Sant'Antonio",
            "codiceCatastale" => "H467"
        ],
        [
            "nome" => "Roseto Valfortore",
            "codiceCatastale" => "H568"
        ],
        [
            "nome" => "San Marco La Catola",
            "codiceCatastale" => "H986"
        ],
        [
            "nome" => "Sannicandro Garganico",
            "codiceCatastale" => "I054"
        ],
        [
            "nome" => "Sant'Agata Di Puglia",
            "codiceCatastale" => "I193"
        ],
        [
            "nome" => "Vico Del Gargano",
            "codiceCatastale" => "L842"
        ],
        [
            "nome" => "Vieste",
            "codiceCatastale" => "L858"
        ],
        [
            "nome" => "Volturara Appula",
            "codiceCatastale" => "M131"
        ],
    ];


    public function list(int $idAnno)
    {
        return IstituzioneScolastica::query()->with(["tipoIstituto", "alunni"])
            ->where("idAnnoScolastico", "=", "?")
            ->setBindings([$idAnno])
            ->get();
    }

    public function istituzioniScolasticheData($istituzioni)
    {
        if ($istituzioni == null) {
            return null;
        }

        if (($istituzioni instanceof Collection)) {

            foreach ($istituzioni as $istituzione) {
                self::assicuraCampi($istituzione);
            }
            return $istituzioni;
        } else {
            $istituzioni_array = [];
            self::assicuraCampi($istituzioni);
            array_push($istituzioni_array, $istituzioni);
            return $istituzioni_array;
        }

    }

    private function assicuraCampi($istituzione)
    {

        $istituzione->alunni;
        $istituzione->tipoIstituto;
        $this->caricaSediCiclo1($istituzione);
        $this->caricaSediCiclo2($istituzione);
    }

    private static function caricaSediCiclo1($istituzione)
    {
        $sediCiclo1Rep = new SediCiclo1Repository();
        $sediCiclo1Rep->sediData($istituzione->sediCiclo1);

    }

    private static function caricaSediCiclo2($istituzione)
    {
        $sediCiclo2Rep = new SediCiclo2Repository();
        $sediCiclo2Rep->sediData($istituzione->sediCiclo2);


    }


    public function cercaByCodice(int $idAnno, string $codiceIS): ?array
    {

        $IS = IstituzioneScolastica::trovaCodiceAnno($codiceIS, $idAnno);

        return $this->istituzioniScolasticheData($IS);


    }

    public function ciclo1(int $idAnno)
    {
        return IstituzioneScolastica::has("sediCiclo1")
            ->where("idAnnoScolastico", "=", "?")
            ->with(
                [
                    "alunni",
                    "sediCiclo1",
                    "sediCiclo1.alunni",
                ])
            ->setBindings([$idAnno])
            ->get();
    }

    public function ciclo2(int $idAnno)
    {
        $ciclo2 = IstituzioneScolastica::has("sediCiclo2")
            ->where("idAnnoScolastico", "=", "?")
            ->with(
                [
                    "alunni",
                    "sediCiclo2",
//                    "sediCiclo2.alunni" => function ($query) use ($idAnno) {
//                        $query->where("idAnnoScolastico", "=", $idAnno);
//                    },
                ])
            ->setBindings([$idAnno])
            ->get();
        foreach ($ciclo2 as $is) {
            /** @var SedeCiclo2 $sede */
            foreach ($is->sediCiclo2 as $sede) {

                $sede->indirizziDiStudio;
                $sede->alunni = $sede->getTotaleAlunniPerSede();

            }
        }
        return $ciclo2;
    }

    public function sovraDimensionate(int $idAnno)
    {

        return DB::table('istituzioniScolastiche')
            ->select("istituzioniScolastiche.id", "codice", "totaleAlunni", "codiceCatastaleComuneDirigenza", "comuneSedeDirigenza")
            ->join('alunniIs', 'istituzioniScolastiche.id', '=', 'alunniIs.idIs')
            ->where('totaleAlunni', '>', '?')
            ->where('idAnnoScolastico', '=', '?')
            ->setBindings([1200, $idAnno])
            ->get();

    }

    public function sottoDimensionate(int $idAnno)
    {
        $nomi = "'" . join("', '", array_map(function ($item) {
                return str_replace("'", "''", $item);
            },
                array_column(self::comuniSvantaggiati, "nome"))) . "'";

        $query = <<<QUERY
SELECT id
FROM   "istituzioniScolastiche"
JOIN   (
   SELECT * FROM unnest(ARRAY[$nomi]) comuni
)c  ON "comuneSedeDirigenza" ILIKE c.comuni
where "idAnnoScolastico" = $idAnno
QUERY;


        $isComuniSvantaggiatiAnnoCorrente = DB::select(DB::raw($query));

        $in = [];
        foreach ($isComuniSvantaggiatiAnnoCorrente as $item) {
            $in[] = $item->id;
        }

        $sottoDimensionateComuneSvantaggiato = DB::table('istituzioniScolastiche')
            ->join('alunniIs', 'istituzioniScolastiche.id', '=', 'alunniIs.idIs')
            ->select("istituzioniScolastiche.id", "codice", "totaleAlunni", 'codiceCatastaleComuneDirigenza', 'comuneSedeDirigenza')
            ->where('totaleAlunni', '<', self::limiteSottoDimensionata(true))
            ->whereIn('istituzioniScolastiche.id', $in)
            ->get();

        $sottoDimensionate = DB::table('istituzioniScolastiche')
            ->join('alunniIs', 'istituzioniScolastiche.id', '=', 'alunniIs.idIs')
            ->select("istituzioniScolastiche.id", "codice", "totaleAlunni", 'codiceCatastaleComuneDirigenza', 'comuneSedeDirigenza')
            ->where('totaleAlunni', '<', self::limiteSottoDimensionata(false))
            ->whereIn('istituzioniScolastiche.id', $in, 'and', true)
            ->get();

        return array_merge($sottoDimensionateComuneSvantaggiato->toArray(), $sottoDimensionate->toArray());

    }

    public static function limiteSovraDimensionata()
    {
        return self::numeroAlunniIsSovraDimensionata;
    }

    public static function limiteSottoDimensionataPerNomeComune(string $nomeComune)
    {
        $comuneSvantaggiato = array_search(
            strtoupper($nomeComune),
            array_map("strtoupper", array_column(self::comuniSvantaggiati, "nome"))
        );
        return self::limiteSottoDimensionata($comuneSvantaggiato);
    }

    public static function limiteSottoDimensionataPerCodiceCatastaleComune(string $codiceComune)
    {
        $comuneSvantaggiato = array_search(
            strtoupper($codiceComune),
            array_map("strtoupper", array_column(self::comuniSvantaggiati, "codiceCatastale"))
        );
        return self::limiteSottoDimensionata($comuneSvantaggiato);
    }


    private static function limiteSottoDimensionata($comuneSvantaggiato)
    {
        if ($comuneSvantaggiato) {
            return self::numeroAlunniIsSottoDimensionataComuneSvantaggiato;
        } else {
            return self::numeroAlunniIsSottoDimensionata;
        }
    }

    public function find(int $idSede)
    {
        $IS = IstituzioneScolastica::query()->find($idSede);
        return $this->istituzioniScolasticheData($IS);
    }

    public function odof(int $idAnno)
    {
        $query = <<<SQL
Select 'I Ciclo'                  as "ciclo",
       "sediCiclo1".codice        as "codicePE",
       "sediCiclo1".denominazione as "denominazionePE",
       i.denominazione            as "denominazioneIS",
       i.codice                   as "codiceIS",
       "sediCiclo1"."codiceEdificio",
       "sediCiclo1"."tipologiaScuola",
       "comuneIs".nome            as "comuneIS",
       "comuneIs".provincia       as "provincia",
       "sediCiclo1".indirizzo     as "indirizzoPE",
       "comuneSede".nome          as "comunePE",
       a."totaleAlunni"           as "totaleAlunni",
       a."totaleClassi"           as "totaleClassi",
       a."totaleAlunniOF"         as "totaleAlunniOF"
from "sediCiclo1"
         join "istituzioniScolastiche" i on "sediCiclo1"."idIstituzioneScolastica" = i.id
         join "alunniSedeCiclo1" a on "sediCiclo1".id = a."idSedeCiclo1"
--          join "tipiIstituto" tI on i."tipoIstituto" = tI.id
         join "tipologieScuola" tS on "sediCiclo1"."tipologiaScuola" = tS."tipologiaScuola"
         join comuni "comuneSede" on "sediCiclo1"."codiceCatastaleComune" = "comuneSede"."codiceCatastale"
         join comuni "comuneIs" on i."codiceCatastaleComuneDirigenza" = "comuneIs"."codiceCatastale"
WHERE "idAnnoScolastico" = :idAnno
UNION
Select 'II Ciclo'                 as "ciclo",
       "sediCiclo2".codice        as "codicePE",
       "sediCiclo2".denominazione as "denominazionePE",
       i.denominazione            as "denominazioneIS",
       i.codice                   as "codiceIS",
       "sediCiclo2"."codiceEdificio",
       "sediCiclo2"."tipologiaScuola",

       "comuneIs".nome            as "comuneIS",
       "comuneIs".provincia       as "provincia",
       "sediCiclo2".indirizzo     as "indirizzoPE",
       "comuneSede".nome          as "comunePE",
       a."totaleAlunni"           as "totaleAlunni",
       a."totaleClassi"           as "totaleClassi",
       a."totaleAlunniOF"         as "totaleAlunniOF"
from "sediCiclo2"
         join "istituzioniScolastiche" i on "sediCiclo2"."idIstituzioneScolastica" = i.id
         join
     (
         -- i dati sugli alunni delle sediCiclo2 vengono memorizzati divisi per edifici,
         -- perciò è necessario aggregarli prima di visualizzarli
         select "idSedeCiclo2",
                sum("totaleClassi")         as "totaleClassi",
                sum("totaleAlunni")         as "totaleAlunni",
                sum("totaleAlunniDisabili") as "totaleAlunniDisabili",
                sum("totaleAlunniOF")       as "totaleAlunniOF"
         from "alunniSedeCiclo2" a
         group by "idSedeCiclo2"
     ) as a on "sediCiclo2".id = a."idSedeCiclo2"
         join "tipologieScuola" tS on "sediCiclo2"."tipologiaScuola" = tS."tipologiaScuola"
         join comuni "comuneSede" on "sediCiclo2"."codiceCatastaleComune" = "comuneSede"."codiceCatastale"
         join comuni "comuneIs" on i."codiceCatastaleComuneDirigenza" = "comuneIs"."codiceCatastale"
WHERE "idAnnoScolastico" = :idAnno
SQL;

        $data = DB::select(DB::raw($query), [':idAnno' => $idAnno]);

        $header = [
            ['name' => "ciclo", 'label' => "Ciclo"],
            ['name' => "codicePE", 'label' => "Codice PE"],
            ['name' => "denominazionePE", 'label' => "Denominazione PE"],
            ['name' => "codiceIS", 'label' => "Codice IS"],
            ['name' => "denominazioneIS", 'label' => "Denominazione IS"],
            ['name' => "codiceEdificio", 'label' => "Codice Edificio"],
            ['name' => "tipologiaScuola", 'label' => "Tipologia Scuola"],
            ['name' => "comuneIS", 'label' => "Comune IS"],
            ['name' => "provincia", 'label' => "Provincia"],
            ['name' => "comunePE", 'label' => "Comune PE"],
            ['name' => "indirizzoPE", 'label' => "Indirizzo PE"],
            ['name' => "totaleAlunni", 'label' => "Totale Alunni OD"],
            ['name' => "totaleClassi", 'label' => "Totale Classi OD"],
            ['name' => "totaleAlunniOF", 'label' => "Totale Alunni OF"]
        ];

        return [
            'header' => $header,
            'data' => $data
        ];

    }


}
