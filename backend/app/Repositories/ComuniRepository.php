<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Repositories;


use App\Models\Comune;
use App\Models\Distanza;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class ComuniRepository
{


    public function comuniData($comuni)
    {
        foreach ($comuni as $comune) {
            $this->comuneData($comune);
        }

        return $comuni;
    }


    public function comuneData($comune)
    {
        $comune->ambito;
        $comune->provincia;
        $is = new ISRepository();
        if ($comune->ambito)
            foreach ($comune->ambito->istituzioniScolastiche as $is) {
                //$is->sediCiclo1;
                //$is->sediCiclo2;
                $is->alunni;
            }


        return $comune;


    }


    public function ambiti()
    {


        return $this->comuniData(Comune::select('nome',
            'provincia',
            'regione',
            'cap', 'ambitoTerritoriale')->get());


    }

    public function lista($soloPuglia = true)
    {
        $command = Comune::query();
        if ($soloPuglia) {
            $command = $command->where(['regione' => 'Puglia']);
        }
        return $command->get(['nome', 'codiceCatastale', 'provincia', 'ambitoTerritoriale']);
    }

    public function infoComune(string $codiceCatastale, bool $includiConfine)
    {

        $campi = [
            "codiceCatastale",
            "nome",
            "provincia",
            "regione",
            "cap",
            "codiceIstat",
            "ambitoTerritoriale"
        ];
        if ($includiConfine) {
            $campi[] = "confine";
        }
        return Comune::query()->find(strtoupper($codiceCatastale), $campi);

    }

    /**
     * @param string $codiceOri
     * @param string $codiceDst
     * @return object|null
     */
    public function distanza(string $codiceOri, string $codiceDst)
    {
        return Distanza::query()->where([
            'codiceCatastaleOrigine' => strtoupper($codiceOri),
            'codiceCatastaleDestinazione' => strtoupper($codiceDst)
        ])->first(["codiceCatastaleOrigine", "codiceCatastaleDestinazione", "minuti"]);
    }


    /**
     * Restituisce una vista dei comuni vicini
     *
     * @param $codiceComuneOrigine string codice catastale comune di origine
     * @param $distanzaMaxMinuti int minuti di distanza massima per considerare il comune vicino
     *
     * @return Collection
     */
    public function vicini(string $codiceComuneOrigine, int $distanzaMaxMinuti)
    {
        return DB::table('distanze')
            ->join('comuni', 'distanze.codiceCatastaleDestinazione', '=', 'comuni.codiceCatastale')
            ->select(
                "codiceCatastale",
                "nome",
                "provincia",
                "regione",
                "cap",
                "codiceIstat",
                "ambitoTerritoriale", 'minuti')
            ->where('codiceCatastaleOrigine', '=', strtoupper($codiceComuneOrigine))
            ->where('minuti', '<=', $distanzaMaxMinuti)
            ->where('codiceCatastale', '!=', $codiceComuneOrigine)
            ->orderBy('minuti')
            ->get();
    }

}
