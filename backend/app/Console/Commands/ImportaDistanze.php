<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Console\Commands;

use App\Models\Distanza;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ImportaDistanze extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'importa:distanze';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importa distanze fra i comuni Pugliesi';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::transaction(function () {
            DB::statement('TRUNCATE ssd.distanze CASCADE');
            $this->importaDistanzeCsv();
        });

    }


    private function importaDistanzeCsv()
    {

        $fileName = __DIR__ . "/../../../dataset/33_Puglia_Distanze.csv";
        $headerChecked = false;

        $csv = array_map('str_getcsv', file($fileName));

        $header = [
            3 => "Total_Minu",
            4 => "Total_Mete",
            9 => "ORIG_CodiceCatastaledelcomune",
            14 => "DEST_CodiceCatastaledelcomune",
        ];
        $this->output->progressStart(count($csv));
        foreach ($csv as $line) {
            if (!$headerChecked) {
                foreach ($header as $key => $item) {
                    if ($line[$key] != $item) {
                        throw new \RuntimeException("Invalid header");
                    }
                }
                $headerChecked = true;
                continue;
            }
            $distanza = new Distanza();
            $distanza->codiceCatastaleOrigine = $line[9];
            $distanza->codiceCatastaleDestinazione = $line[14];
            $distanza->minuti = floatval($line[3]);
            $distanza->mete = floatval($line[4]);
            $distanza->save();
//            $this->info($distanza->codiceCatastaleOrigine." -> ".$distanza->codiceCatastaleDestinazione ." importata!" );
            $this->output->progressAdvance();
        }
        $this->output->progressFinish();

    }
}
