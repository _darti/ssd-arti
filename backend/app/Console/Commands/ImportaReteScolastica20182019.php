<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Console\Commands;

use App\Helpers\ImportHelpers;
use App\Helpers\ImportTraits;
use App\Models\AnnoScolastico;
use App\Models\IndirizzoStudioCiclo2;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ImportaReteScolastica20182019 extends Command
{

    use ImportTraits;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'importa:reteScolastica20182019';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';


    const HEADER_A1_CODICE = "Codice";
    const HEADER_B1_DENOMINAZIONE = "Denominazione";
    const HEADER_C1_CODICE = "Cod. ist. Rif.";
    const HEADER_ROW_IDX = 1;


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return int
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws Exception
     */
    public function handle()
    {

        $this->eliminaAnnoScolastico("2018/2019");
        $annoScolastico = AnnoScolastico::nuovoAnnoScolastico("2018/2019");
        $this->info("IS");
        $this->handleIs($annoScolastico);
        $this->info("Sedi");
        $this->handleSedi($annoScolastico);
        $this->info("Dettagli alunni");
        $this->importaDettagliAlunni($annoScolastico);
        $this->output->success("Done");

        return 1;

    }


    private function handleIs(AnnoScolastico $annoScolastico)
    {
        //nelle prossime versioni potrebbe essere preso da stdin
        $fileName = __DIR__ . "/../../../dataset/35_Rete scolastica 2018-19.xlsx";
        //
        $istituzioniScolastiche = $this->parse2018($fileName);

        $this->importaTipiIstituti($istituzioniScolastiche);


        DB::beginTransaction();
        foreach ($istituzioniScolastiche as $is) {
            $this->inserisciIS($is, $annoScolastico);
        }
        DB::commit();
    }

    /**
     * @param $fileName
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function parse2018($fileName)
    {
        //0. load file
        $reader = IOFactory::createReaderForFile($fileName);
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileName);

        $istituzioniScolastiche = [];
        $worksheet = $spreadsheet->getSheet(4);
        {
            $HEADER_ROW_IDX = 2;

            $checks = [
                'C2' => "Ambito",
                'G2' => "CODICE",
                'D2' => "Descrizione",
            ];

            //1. perform "format" check
            ImportHelpers::performFormatCheck($worksheet, $checks);

            //2. parse data
            foreach ($worksheet->getRowIterator($HEADER_ROW_IDX + 1) as $row) {
                $rowIndex = $row->getRowIndex();
                $codiceIS = $worksheet->getCellByColumnAndRow(7, $rowIndex)->getValue();
                if (is_null($codiceIS) || $codiceIS == "") {
                    break;
                }
                $tipoIS = $worksheet->getCellByColumnAndRow(6, $rowIndex)->getValue();
                $denominazione = $worksheet->getCellByColumnAndRow(8, $rowIndex)->getValue();
                $comuneSedeDirigenza = $worksheet->getCellByColumnAndRow(5, $rowIndex)->getValue();
                $multiAmbito = false;
                $codiceAmbito = $worksheet->getCellByColumnAndRow(4, $rowIndex)->getValue();

//                print "c: $codiceIS, tIS: $tipoIS, d: $denominazione, multia: $multiAmbito, ca: $codiceAmbito \n";
                $istituzioniScolastiche[] = [
                    'codiceIS' => $codiceIS,
                    'tipoIS' => $tipoIS,
                    'comuneSedeDirigenza' => $comuneSedeDirigenza,
                    'denominazioneIs' => $denominazione,
                    'multiAmbito' => $multiAmbito,
                    'codiceAmbito' => $codiceAmbito
                ];
            }
        }
        return $istituzioniScolastiche;
    }


    /**
     *
     * @param AnnoScolastico $annoScolastico
     * @throws Exception
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function handleSedi(AnnoScolastico $annoScolastico)
    {

        $fileName = __DIR__ . "/../../../dataset/Rete scolastica 2018-19_coordinate e sedi ammin-rev1.xls";
        //0. load file
        $reader = IOFactory::createReaderForFile($fileName);
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileName);
        $this->info("======================= Ciclo 1\n\n");
        if (true) {
            $worksheetCiclo1 = $spreadsheet->getSheet(0);
            $sediData = $this->parseSediCiclo1($worksheetCiclo1);
            foreach ($sediData as $sedeData) {
                $this->inserisciSedeCiclo1($sedeData, $annoScolastico);
            }


        }


        {
            $this->info("======================= Ciclo 2\n\n");

            $indirizziDiStudioNonTrovati = [];
            $worksheetCiclo2 = $spreadsheet->getSheet(1);

            $sediData = $this->parseSediCiclo2($worksheetCiclo2);

            foreach ($sediData as $sedeData) {

                $sedeModel = $this->inserisciSedeCiclo2($sedeData, $annoScolastico);
                if ($sedeModel == null) {
                    continue;
                }


                $indirizziDiStudio = $sedeData['indirizziDiStudio'];
                foreach ($indirizziDiStudio as $codice) {
                    $indirizzoDiStudioModel = IndirizzoStudioCiclo2::query()->whereKey($codice)->first();
                    if ($indirizzoDiStudioModel == null) {
                        $indirizziDiStudioNonTrovati[] = $codice;
                    } else {
                        $sedeModel->indirizziDiStudio()->attach($indirizzoDiStudioModel);
                    }


                }


            }

            $report = array_unique($indirizziDiStudioNonTrovati);
            if (!empty($report)) {
                $this->error("indirizzi di studio non trovati: " . join(", ", $report));
            }

        }

    }

    /**
     * @param $worksheetCiclo1 Worksheet
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function parseSediCiclo1($worksheetCiclo1)
    {
        //1. perform "format" check
        $checks = [
            'A1' => self::HEADER_A1_CODICE,
            'B1' => self::HEADER_B1_DENOMINAZIONE,
            'C1' => self::HEADER_C1_CODICE,
//            'E1' => self::HEADER_E1_NOTE

        ];
        ImportHelpers::performFormatCheck($worksheetCiclo1, $checks);

        $ciclo1 = [];

        //2. parse data
        foreach ($worksheetCiclo1->getRowIterator(self::HEADER_ROW_IDX + 1) as $row) {
            $rowIndex = $row->getRowIndex();
            $codiceSede = $worksheetCiclo1->getCellByColumnAndRow(1, $rowIndex)->getValue();
            if (is_null($codiceSede) || $codiceSede == "") {
                break;
            }
            $denominazione = $worksheetCiclo1->getCellByColumnAndRow(2, $rowIndex)->getCalculatedValue();
            $codiceIS = $worksheetCiclo1->getCellByColumnAndRow(3, $rowIndex)->getValue();
            $tipo = $worksheetCiclo1->getCellByColumnAndRow(5, $rowIndex)->getValue();
            $isDirezioneAmministrativa = (boolean)$worksheetCiclo1->getCellByColumnAndRow(6, $rowIndex)->getValue() != "";
            $verticalizzazione = $worksheetCiclo1->getCellByColumnAndRow(7, $rowIndex)->getValue();
            $tipologiaScuola = trim($worksheetCiclo1->getCellByColumnAndRow(8, $rowIndex)->getValue());
            $codiceEdificio = $worksheetCiclo1->getCellByColumnAndRow(9, $rowIndex)->getValue();
            $lat = $worksheetCiclo1->getCellByColumnAndRow(10, $rowIndex)->getValue();
            $lon = $worksheetCiclo1->getCellByColumnAndRow(11, $rowIndex)->getValue();
            $comuneSede = $worksheetCiclo1->getCellByColumnAndRow(12, $rowIndex)->getCalculatedValue();
            $indirizzoPlesso = $worksheetCiclo1->getCellByColumnAndRow(14, $rowIndex)->getValue();

            $lat = ImportHelpers::isCampoNonDisponibile($lat) ? null : $lat;
            $lon = ImportHelpers::isCampoNonDisponibile($lon) ? null : $lon;

            $ciclo1[] = compact(
                'codiceSede',
                'denominazione',
                'codiceIS',
                'tipo',
                'isDirezioneAmministrativa',
                'verticalizzazione',
                'tipologiaScuola',
                'codiceEdificio',
                'lat',
                'lon',
                'comuneSede',
                'indirizzoPlesso',
                'denominazione');
        }
        return $ciclo1;
    }


    /**
     * @param $worksheetCiclo2 Worksheet
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function parseSediCiclo2($worksheetCiclo2)
    {
        //1. perform "format" check

        $checks = [
            'A1' => self::HEADER_A1_CODICE,
            'B1' => self::HEADER_B1_DENOMINAZIONE,
            'C1' => self::HEADER_C1_CODICE,
//            'E1' => self::HEADER_E1_NOTE
        ];
        ImportHelpers::performFormatCheck($worksheetCiclo2, $checks);

        $ciclo2 = [];

        //2. parse data
        foreach ($worksheetCiclo2->getRowIterator(self::HEADER_ROW_IDX + 1) as $row) {
            $rowIndex = $row->getRowIndex();
            $codiceSede = $worksheetCiclo2->getCellByColumnAndRow(1, $rowIndex)->getValue();
            if (is_null($codiceSede) || $codiceSede == "") {
                break;
            }
            $denominazione = $worksheetCiclo2->getCellByColumnAndRow(2, $rowIndex)->getCalculatedValue();
            $codiceIS = $worksheetCiclo2->getCellByColumnAndRow(3, $rowIndex)->getValue();
            $tipo = $worksheetCiclo2->getCellByColumnAndRow(5, $rowIndex)->getValue();
            $isDirezioneAmministrativa = (boolean)$worksheetCiclo2->getCellByColumnAndRow(6, $rowIndex)->getValue() != "";
            $verticalizzazione = $worksheetCiclo2->getCellByColumnAndRow(7, $rowIndex)->getValue();
            $tipologiaScuola = trim($worksheetCiclo2->getCellByColumnAndRow(8, $rowIndex)->getValue());
            $codiceEdificio = $worksheetCiclo2->getCellByColumnAndRow(9, $rowIndex)->getValue();
            $lat = $worksheetCiclo2->getCellByColumnAndRow(10, $rowIndex)->getValue();
            $lon = $worksheetCiclo2->getCellByColumnAndRow(11, $rowIndex)->getValue();
            $comuneSede = $worksheetCiclo2->getCellByColumnAndRow(12, $rowIndex)->getCalculatedValue();
            $indirizzoPlesso = $worksheetCiclo2->getCellByColumnAndRow(14, $rowIndex)->getValue();

            $lat = ImportHelpers::isCampoNonDisponibile($lat) ? null : $lat;
            $lon = ImportHelpers::isCampoNonDisponibile($lon) ? null : $lon;

            $indirizziDiStudio = [];
            for ($columnIndex = 17; $columnIndex <= 31; $columnIndex++) {
                $indirizzo = $worksheetCiclo2->getCellByColumnAndRow($columnIndex, $rowIndex)->getValue();
                if (!ImportHelpers::isCampoNonDisponibile($indirizzo)) {
                    $indirizziDiStudio[] = $indirizzo;
                }
            }

            $ciclo2[] = compact(
                'codiceSede',
                'denominazione',
                'codiceIS',
                'tipo',
                'isDirezioneAmministrativa',
                'verticalizzazione',
                'tipologiaScuola',
                'codiceEdificio',
                'lat',
                'lon',
                'comuneSede',
                'indirizzoPlesso',
                'denominazione',
                'indirizziDiStudio');
        }
        return $ciclo2;
    }


    private function importaDettagliAlunni(AnnoScolastico $annoScolastico)
    {
        //nelle prossime versioni potrebbe essere preso da stdin
        $fileName = __DIR__ . "/../../../dataset/35_Rete scolastica 2018-19.xlsx";

        $this->output->progressStart();

        $reader = IOFactory::createReaderForFile($fileName);
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileName);


        $dataIs = $this->ottieniAlunniTotaliPerIs($spreadsheet);
        $this->output->progressAdvance();
        $this->info("Alunni IS");

        foreach ($dataIs as $is) {
            try {
                $this->inserisciAlunniIs($is, $annoScolastico);
            } catch (\Exception $exception) {
                $this->error($exception->getMessage());
            }
        }
        $this->info("Alunni Infanzia");
        $datiInfanzia = $this->ottieniDatiInfanzia($spreadsheet);
        foreach ($datiInfanzia as $item) {
            try {
                $this->inserisciAlunniCiclo1($item, $annoScolastico);
            } catch (\Exception $exception) {
                $this->error($exception->getMessage());
            }
        }
        $this->output->progressAdvance();

        $this->info("Alunni Primaria");
        $datiPrimaria = $this->ottieniDatiPrimaria($spreadsheet);
        foreach ($datiPrimaria as $item) {
            try {
                $this->inserisciAlunniCiclo1($item, $annoScolastico);
            } catch (\Exception $exception) {
                $this->error($exception->getMessage());
            }
        }
        $this->output->progressAdvance();
        //grado 1
        $this->info("Alunni Grado 1");
        $datiGrado1 = $this->ottieniDatiGrado1($spreadsheet);
        foreach ($datiGrado1 as $item) {
            try {
                $this->inserisciAlunniCiclo1($item, $annoScolastico);
            } catch (\Exception $exception) {
                $this->error($exception->getMessage());
            }
        }
        $this->output->progressAdvance();


        //grado 2
        $error = 0;
        $this->info("Alunni Grado 2");
        $datiGrado2 = $this->ottieniDatiGrado2($spreadsheet);
        foreach ($datiGrado2 as $item) {
            try {
                $this->inserisciAlunniCiclo2($item, $annoScolastico);
            } catch (\Exception $exception) {
                $this->error($exception->getMessage());
                $error++;
            }
        }

        if ($error != 0) {
            $this->error("Si sono verificati $error errori");
        }

        $this->output->progressFinish();
    }

    /**
     * @param Spreadsheet $spreadsheet
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function ottieniAlunniTotaliPerIs(Spreadsheet $spreadsheet)
    {


        $dataIs = [];
        $worksheet = $spreadsheet->getSheet(4);
        {
            $HEADER_ROW_IDX = 2;

            $checks = [
                'C2' => "Ambito",
                'G2' => "CODICE",
                'D2' => "Descrizione",
            ];

            //1. perform "format" check
            ImportHelpers::performFormatCheck($worksheet, $checks);

            //2. parse data
            foreach ($worksheet->getRowIterator($HEADER_ROW_IDX + 1) as $row) {
                $rowIndex = $row->getRowIndex();
                $codiceIS = $worksheet->getCellByColumnAndRow(7, $rowIndex)->getValue();
                if (ImportHelpers::checkIfEmptyCell($codiceIS)) {
                    continue;
                }

                $nAlunni = $worksheet->getCellByColumnAndRow(12, $rowIndex)->getValue();
                $nDisabili = $worksheet->getCellByColumnAndRow(13, $rowIndex)->getValue();
                $nClassi = $worksheet->getCellByColumnAndRow(14, $rowIndex)->getValue();


                $dataIs[] = compact('codiceIS', 'nAlunni', 'nClassi', 'nDisabili');
            }
        }
        return $dataIs;
    }

    /**
     * @param Spreadsheet $spreadsheet
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function ottieniDatiInfanzia(Spreadsheet $spreadsheet)
    {
        $dataInfanzia = [];
        $worksheet = $spreadsheet->getSheet(0);
        {
            $HEADER_ROW_IDX = 2;

            $checks = [
                'J1' => "Totale",
                'M1' => "Orario normale",
                'O1' => "Orario ridotto",
                'Q1' => "Bambini per età",
                'C2' => "Codice",
                'G2' => "Cod. ist. Rif.",
                'J2' => "Bambini",
                'K2' => "di cui: con hand.",
                'L2' => 'Sezioni',
                'M2' => "Bambini",
                "N2" => "Sezioni",
            ];

            //1. perform "format" check
            ImportHelpers::performFormatCheck($worksheet, $checks);

            //2. parse data
            foreach ($worksheet->getRowIterator($HEADER_ROW_IDX + 1) as $row) {
                $rowIndex = $row->getRowIndex();
                $codiceSede = $worksheet->getCell("C$rowIndex")->getValue();
                if (ImportHelpers::checkIfEmptyCell($codiceSede)) {
                    continue;
                }

                $nAlunni = $worksheet->getCell("J$rowIndex")->getValue();
                $nDisabili = $worksheet->getCell("K$rowIndex")->getValue();
                $nClassi = $worksheet->getCell("L$rowIndex")->getValue();

                $dettagli = [];
                $dettagli['orarioNormale']['nAlunni'] = $worksheet->getCell("M$rowIndex")->getValue();
                $dettagli['orarioNormale']['sezioni'] = $worksheet->getCell("N$rowIndex")->getValue();
                $dettagli['orarioRidotto']['nAlunni'] = $worksheet->getCell("O$rowIndex")->getValue();
                $dettagli['orarioRidotto']['sezioni'] = $worksheet->getCell("P$rowIndex")->getValue();
                $dettagli['etaAlunni']['meno4anni'] = $worksheet->getCell("Q$rowIndex")->getValue();
                $dettagli['etaAlunni']['4o5anni'] = $worksheet->getCell("R$rowIndex")->getValue();
                $dettagli['etaAlunni']['piu5anni'] = $worksheet->getCell("S$rowIndex")->getValue();

                $dataInfanzia[] = compact('codiceSede', 'nAlunni', 'nClassi', 'nDisabili', 'dettagli');
            }
        }
        return $dataInfanzia;

    }


    /**
     * @param Spreadsheet $spreadsheet
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function ottieniDatiPrimaria(Spreadsheet $spreadsheet)
    {
        $dataInfanzia = [];
        $worksheet = $spreadsheet->getSheet(1);
        {
            $HEADER_ROW_IDX = 3;

            $checks = [
                'C3' => "Codice",
                'J2' => "Totale",
                'J3' => "Alunni",
                'K3' => "di cui con hand.",
                'L3' => "Classi",
                'M3' => "Alunni",
                'P3' => "Alunni",
                'S3' => "Alunni",
                'V3' => "Alunni",
                'Z3' => "Alunni"
            ];

            //1. perform "format" check
            ImportHelpers::performFormatCheck($worksheet, $checks);

            //2. parse data
            foreach ($worksheet->getRowIterator($HEADER_ROW_IDX + 1) as $row) {
                $rowIndex = $row->getRowIndex();
                $codiceSede = $worksheet->getCell("C$rowIndex")->getValue();
                if (ImportHelpers::checkIfEmptyCell($codiceSede)) {
                    continue;
                }

                $nAlunni = $worksheet->getCell("J$rowIndex")->getValue();
                $nDisabili = $worksheet->getCell("K$rowIndex")->getValue();
                $nClassi = $worksheet->getCell("L$rowIndex")->getValue();

                $dettagli = [];
                //1 anno
                $dettagli['1anno']['nAlunni'] = $worksheet->getCell("M$rowIndex")->getValue();
                $dettagli['1anno']['nDisabili'] = $worksheet->getCell("N$rowIndex")->getValue();
                $dettagli['1anno']['classi'] = $worksheet->getCell("O$rowIndex")->getValue();
                //2 anno
                $dettagli['2anno']['nAlunni'] = $worksheet->getCell("P$rowIndex")->getValue();
                $dettagli['2anno']['nDisabili'] = $worksheet->getCell("Q$rowIndex")->getValue();
                $dettagli['2anno']['classi'] = $worksheet->getCell("R$rowIndex")->getValue();
                //3 anno
                $dettagli['3anno']['nAlunni'] = $worksheet->getCell("S$rowIndex")->getValue();
                $dettagli['3anno']['nDisabili'] = $worksheet->getCell("T$rowIndex")->getValue();
                $dettagli['3anno']['classi'] = $worksheet->getCell("U$rowIndex")->getValue();
                //4 anno
                $dettagli['4anno']['nAlunni'] = $worksheet->getCell("V$rowIndex")->getValue();
                $dettagli['4anno']['nDisabili'] = $worksheet->getCell("W$rowIndex")->getValue();
                $dettagli['4anno']['classi'] = $worksheet->getCell("X$rowIndex")->getValue();
                $dettagli['4anno']['rapporto'] = $worksheet->getCell("Y$rowIndex")->getValue();
                //5 anno
                $dettagli['5anno']['nAlunni'] = $worksheet->getCell("Z$rowIndex")->getValue();
                $dettagli['5anno']['nDisabili'] = $worksheet->getCell("AA$rowIndex")->getValue();
                $dettagli['5anno']['classi'] = $worksheet->getCell("AB$rowIndex")->getValue();
                $dettagli['5anno']['plur'] = $worksheet->getCell("AC$rowIndex")->getValue();


                $dataInfanzia[] = compact('codiceSede', 'nAlunni', 'nClassi', 'nDisabili', 'dettagli');
            }
        }
        return $dataInfanzia;

    }


    /**
     * @param Spreadsheet $spreadsheet
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function ottieniDatiGrado1(Spreadsheet $spreadsheet)
    {
        $dataGrado1 = [];
        $worksheet = $spreadsheet->getSheet(2);
        {
            $HEADER_ROW_IDX = 3;

            $checks = [
                'C2' => "Codice",
                'K2' => "Totale",
                'K3' => "Alunni",
                'L3' => "Hand",
                'M3' => "Classi",
                'N3' => "Alunni",
                'O3' => "Hand",
                'P3' => "Classi",
                'Q3' => "Alunni",
                'R3' => "Hand",
                'S3' => "Classi",
                'T3' => "Alunni",
                'U3' => "Hand",
                'V3' => "Classi",

            ];

            //1. perform "format" check
            ImportHelpers::performFormatCheck($worksheet, $checks);

            //2. parse data
            foreach ($worksheet->getRowIterator($HEADER_ROW_IDX + 1) as $row) {
                $rowIndex = $row->getRowIndex();
                $codiceSede = $worksheet->getCell("C$rowIndex")->getValue();
                if (ImportHelpers::checkIfEmptyCell($codiceSede)) {
                    continue;
                }

                $nAlunni = $worksheet->getCell("K$rowIndex")->getValue();
                $nDisabili = $worksheet->getCell("L$rowIndex")->getValue();
                $nClassi = $worksheet->getCell("M$rowIndex")->getValue();

                $dettagli = [];
                //1 anno
                $dettagli['1anno']['nAlunni'] = $worksheet->getCell("N$rowIndex")->getValue();
                $dettagli['1anno']['nDisabili'] = $worksheet->getCell("O$rowIndex")->getValue();
                $dettagli['1anno']['classi'] = $worksheet->getCell("P$rowIndex")->getValue();
                //2 anno
                $dettagli['2anno']['nAlunni'] = $worksheet->getCell("Q$rowIndex")->getValue();
                $dettagli['2anno']['nDisabili'] = $worksheet->getCell("R$rowIndex")->getValue();
                $dettagli['2anno']['classi'] = $worksheet->getCell("S$rowIndex")->getValue();
                //3 anno
                $dettagli['3anno']['nAlunni'] = $worksheet->getCell("T$rowIndex")->getValue();
                $dettagli['3anno']['nDisabili'] = $worksheet->getCell("U$rowIndex")->getValue();
                $dettagli['3anno']['classi'] = $worksheet->getCell("V$rowIndex")->getValue();


                $dataGrado1[] = compact('codiceSede', 'nAlunni', 'nClassi', 'nDisabili', 'dettagli');
            }
        }
        return $dataGrado1;

    }


    /**
     * @param Spreadsheet $spreadsheet
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function ottieniDatiGrado2(Spreadsheet $spreadsheet)
    {
        $dataGrado2 = [];
        $worksheetGrado2 = $spreadsheet->getSheet(3);

        $HEADER_ROW_IDX = 1;

        $checksGrado2 = [
            'F1' => "Codice",
            'J1' => 'Cod. ind.',
            'K1' => "Denominazione indirizzo",
            'L1' => "al1",
            'M1' => "cl1",
            'N1' => "al2",
            'O1' => "cl2",
            'P1' => "al3",
            'Q1' => "cl3",
            'R1' => "al4",
            'S1' => "cl4",
            'T1' => "al5",
            'U1' => "cl5",
            'V1' => "al6",
            'W1' => "cl6",
        ];

        ImportHelpers::performFormatCheck($worksheetGrado2, $checksGrado2);

        //2. parse data
        foreach ($worksheetGrado2->getRowIterator($HEADER_ROW_IDX + 1) as $row) {
            $rowIndex = $row->getRowIndex();
            $codiceSede = $worksheetGrado2->getCell("F$rowIndex")->getValue();
            if (ImportHelpers::checkIfEmptyCell($codiceSede)) {
                continue;
            }
            $codiceIndirizzo = $worksheetGrado2->getCell("J$rowIndex")->getValue();

            $dettagli = [];
            //1 anno
            $dettagli['1anno']['nAlunni'] = $worksheetGrado2->getCell("L$rowIndex")->getValue();
            $dettagli['1anno']['nClassi'] = $worksheetGrado2->getCell("M$rowIndex")->getValue();
            //2 anno
            $dettagli['2anno']['nAlunni'] = $worksheetGrado2->getCell("N$rowIndex")->getValue();
            $dettagli['2anno']['nClassi'] = $worksheetGrado2->getCell("O$rowIndex")->getValue();
            //3 anno
            $dettagli['3anno']['nAlunni'] = $worksheetGrado2->getCell("P$rowIndex")->getValue();
            $dettagli['3anno']['nClassi'] = $worksheetGrado2->getCell("Q$rowIndex")->getValue();
            //4 anno
            $dettagli['4anno']['nAlunni'] = $worksheetGrado2->getCell("R$rowIndex")->getValue();
            $dettagli['4anno']['nClassi'] = $worksheetGrado2->getCell("S$rowIndex")->getValue();
            //5 anno
            $dettagli['5anno']['nAlunni'] = $worksheetGrado2->getCell("T$rowIndex")->getValue();
            $dettagli['5anno']['nClassi'] = $worksheetGrado2->getCell("U$rowIndex")->getValue();
            //6 anno
            $dettagli['6anno']['nAlunni'] = $worksheetGrado2->getCell("V$rowIndex")->getValue();
            $dettagli['6anno']['nClassi'] = $worksheetGrado2->getCell("W$rowIndex")->getValue();
            //$dettagli['totale']['nAlunni'] = $worksheetGrado2->getCell("X$rowIndex")->getCalculatedValue();

            $nClassi = 0;
            $nAlunni = 0;
            $nDisabili = null;


            $nAlunni += $dettagli['1anno']['nAlunni'];
            $nClassi += $dettagli['1anno']['nClassi'];
            //2 anno
            $nAlunni += $dettagli['2anno']['nAlunni'];
            $nClassi += $dettagli['2anno']['nClassi'];
            //3 anno
            $nAlunni += $dettagli['3anno']['nAlunni'];
            $nClassi += $dettagli['3anno']['nClassi'];
            //4 anno
            $nAlunni += $dettagli['4anno']['nAlunni'];
            $nClassi += $dettagli['4anno']['nClassi'];
            //5 anno
            $nAlunni += $dettagli['5anno']['nAlunni'];
            $nClassi += $dettagli['5anno']['nClassi'];
            //6 anno
            $nAlunni += $dettagli['6anno']['nAlunni'];
            $nClassi += $dettagli['6anno']['nClassi'];


            $dataGrado2[] = compact('codiceSede', 'codiceIndirizzo', 'nAlunni', 'nClassi', 'nDisabili', 'dettagli');
        }

        return $dataGrado2;

    }


}
