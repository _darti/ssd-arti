<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Console\Commands;

use App\Helpers\ImportHelpers;
use App\Models\AnnoScolastico;
use App\Models\EdificioNonUtilizzato;
use Illuminate\Console\Command;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Exception;

class ImportaEdificiNonUtilizzati extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'importa:edificiNonUtilizzati';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importa gli edifici non utilizzati';


    private const HEADER_ROW_IDX = 1;

    private const HEADER = [
        'A' => "Lat",
        'B' => "Lon",
        'E' => "CodED1819",

        'K' => "DenPE1920",
        'L' => "ComunePE",
        'S' => "NOTE "
    ];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws Exception
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function handle()
    {


        //
        DB::beginTransaction();
        DB::statement('delete from ssd."edificiNonUtilizzati"');
        $annoScolastico = AnnoScolastico::nuovoAnnoScolastico("2019/2020");
        $this->info("Importing edifici non utilizzati");
        $edifici = $this->leggiEdificiDaXls();
        foreach ($edifici as $edificio) {
            $this->inserisciEdificio($edificio, $annoScolastico);
        }
        DB::commit();
        $this->output->success("Done.");
        return 1;
    }


    /**
     *
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws Exception
     */
    private function leggiEdificiDaXls()
    {


        $fileName = __DIR__ . "/../../../dataset/20192020/Edifici non utilizzati.xlsx";
        //0. load file
        $reader = IOFactory::createReaderForFile($fileName);
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileName);


        $worksheet = $spreadsheet->getSheet(0);


        ImportHelpers::performFormatCheck($worksheet, self::HEADER);
        //2. parse data
        $ambiti = [];

        foreach ($worksheet->getRowIterator(self::HEADER_ROW_IDX + 1) as $row) {
            $rowIndex = $row->getRowIndex();
            $lat = $worksheet->getCell("A$rowIndex")->getValue();
            if (is_null($lat) || $lat == "") {
                break;
            }
            $lon = $worksheet->getCell("B$rowIndex")->getValue();

            $codiceEdificio = $worksheet->getCell("E$rowIndex")->getValue();
            $indirizzo = $worksheet->getCell("K$rowIndex")->getValue();
            $comune = $worksheet->getCell("L$rowIndex")->getValue();
            $note = $worksheet->getCell("S$rowIndex")->getValue() ?: "";

            $ambiti[$codiceEdificio] = [
                'lat' => $lat,
                'lon' => $lon,
                'codiceEdificio' => $codiceEdificio,
                'indirizzo' => $indirizzo,
                'comune' => $comune,
                'note' => $note
            ];
        }
        return $ambiti;

    }


    private function inserisciEdificio($edificioData, AnnoScolastico $annoScolastico)
    {

        $codiceCatastale = @ImportHelpers::comuneByNome($edificioData['comune'])->codiceCatastale ?: null;


        $edificio = new EdificioNonUtilizzato();
        $edificio->idAnnoScolastico = $annoScolastico->id;
        $edificio->codiceEdificio = $edificioData['codiceEdificio'];
        $edificio->coordinate = ImportHelpers::makePoint($edificioData['lat'], $edificioData['lon']);
        $edificio->indirizzo = $edificioData['indirizzo'];
        $edificio->codiceCatastaleComune = $codiceCatastale;
        $edificio->comune = $edificioData['comune'];
        $edificio->note = $edificioData['note'];
        try {
            $edificio->save();
        } catch (QueryException $e) {
            $this->error($e->getMessage());
            print "\n";
        }
    }

}
