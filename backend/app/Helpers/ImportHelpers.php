<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Helpers;


use App\Models\Comune;
use Phaza\LaravelPostgis\Geometries\Point;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use RuntimeException;

class ImportHelpers
{
    static $campoNonValido = ['#N/A', '0'];

    /**
     * Effettua un controllo sull'header del documento per assicurarsi che sia del formato che ci si aspetta.
     *
     * @param Worksheet $worksheet documento
     *
     * @param $header
     * @param int $rowHeader
     * @throws Exception
     */
    public static function performFormatCheck(Worksheet $worksheet, $header, $rowHeader = 1)
    {

        foreach ($header as $coordinate => $expected) {
            if (strlen($coordinate) == 1) {
                $coordinate = "$coordinate$rowHeader";
            }
            $value = $worksheet->getCell($coordinate)->getValue();
            if ($value != $expected) {
                throw new RuntimeException("Format check failed on $coordinate (Expected \"$expected\", got \"$value\") ");
            }
        }

    }

    public static function checkIfEmptyCell(?string $cell)
    {
        return (is_null($cell) || $cell == "");
    }

    public static function isCampoNonDisponibile($val): bool
    {
        return in_array($val, self::$campoNonValido);


    }

    public static function makePoint($lan, $lon): ?Point
    {
        if ($lan == null || $lon == null) {
            return null;
        } else {
            return new Point($lan, $lon);
        }
    }

    public static function comuneByNome($nomeComune): ?Comune
    {
        if ($nomeComune == null || $nomeComune == "") {
            return null;
        }

        $nomeComune = str_replace("`", "'", $nomeComune);
        switch ($nomeComune) {

            case "NARDO'":
                $nomeComune = "Nardò";
                break;
            case "PORTOCESAREO":
                $nomeComune = "Porto Cesareo";
                break;
            case "PATU'":
                $nomeComune = "Patù";
                break;
            case "SECLI'":
                $nomeComune = "Seclì";
                break;
            case "ACQUARICA DEL CAPO":
            case "PRESICCE":
                $nomeComune = "Presicce – Acquarica";
                break;
        }
        $comune = Comune::query()->where('nome', 'ilike', $nomeComune)->first();
        if ($comune == null) {
            print "$nomeComune non trovato\n";
        }
        return $comune;

    }

    public static function comuneByPoint($point): ?Comune
    {

//        $ambiti = Comune::select(["codice", "provincia", "numero", DB::raw("ST_AsGeoJSON(confine) AS confine2")])->get();


//        $comune = Comune::query()->where('confine', ST_Contains::, $nomeComune)->first();
        return null;
    }

    public static function checkNd($in)
    {
        $nds = [
            '',
            'n/d',
            'nd',
            'na'
        ];
        if ($in != null && (is_string($in) && in_array($in, $nds))) {
            return null;
        }
        return $in;
    }
}
