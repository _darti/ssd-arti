<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

/**
 * Created by Reliese Model.
 * Date: Wed, 03 Apr 2019 23:14:26 +0000.
 */

namespace App\Models;

use GeoJson\Geometry\Point;
use Illuminate\Database\Eloquent\Collection;
use Phaza\LaravelPostgis\Eloquent\PostgisTrait;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class CPIAPuntiErogazione1Livello
 *
 * @property int $id
 * @property string $codiceMeccanograficoPuntoErogazione
 * @property string $idCPIA
 * @property int $idTipoSede
 * @property string $denominazione
 * @property string $provincia
 * @property string $comune
 * @property string $codicCatastaleComune
 * @property string $indirizzo
 * @property Point $coordinate
 *
 * @property CPIA $CPIA
 * @property Collection $percorsiErogati
 *
 * @package App\Models
 */
class CPIAPuntoErogazioneLivello1 extends Eloquent
{
    use PostgisTrait;

    protected $table = 'CPIAPuntiErogazioneLivello1';


    public $timestamps = false;
    public static $snakeAttributes = false;

    protected $postgisFields = [
        'coordinate',
    ];

    protected $postgisTypes = [

        'coordinate' => [
            'geomtype' => 'geometry',
            'srid' => 27700
        ]
    ];

    protected $casts = [
        'coordinate' => 'geometry'
    ];

    protected $fillable = [
        'codiceCPIA',
        'denominazione',
        'provincia',
        'comune',
        'indirizzo',
        'coordinate'
    ];

    public function CPIA()
    {
        return $this->belongsTo(CPIA::class, 'codiceCPIA');
    }

    public function percorsiErogati()
    {
        return $this
            ->belongsToMany(
                CPIATipoPercorsoLivello1::class,
                'CPIAPuntiErogazioneLivello1HasCPITipiPercorsoLivello1',
                'CPIAPuntiErogazioneLivello1_id',
                'CPIATipiPercorsoLivello1_id')
            ->withPivot('nPattiFormativi', 'nDiversamenteAbili');
    }
}
