<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AlunniSedeCiclo2
 * @package App\Models
 *
 * @property int $id
 * @property string $idSedeCiclo2
 * @property string $codiceIndirizzo
 * @property int $totaleAlunni
 * @property string $totaleAlunniOF
 * @property int $totaleAlunniDisabili
 * @property int $totaleClassi
 * @property array dettagli
 */
class AlunniSedeCiclo2 extends Model
{
    protected $table = "alunniSedeCiclo2";

    public $timestamps = false;


    protected $casts = [
        'dettagli' => 'array'
    ];

    public static function nuovo($codiceSede, $codiceIndirizzo, AnnoScolastico $annoScolastico): ?AlunniSedeCiclo2
    {
        $sede2 = SedeCiclo2::trovaCodiceAnno($codiceSede, $annoScolastico->id);
        if (!$sede2) {
            throw new Exception("Sede $codiceSede non trovata per anno scolastico $annoScolastico->label ");
        }

        $model = AlunniSedeCiclo2::query()->where([
            'idSedeCiclo2' => $sede2->id,
            'codiceIndirizzo' => $codiceIndirizzo
        ])->first();

        if ($model == null) {
            $model = new AlunniSedeCiclo2();
            $model->idSedeCiclo2 = $sede2->id;
            $model->codiceIndirizzo = $codiceIndirizzo;
            $model->save();
        }
        return $model;
    }
}
