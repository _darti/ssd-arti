<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlunniSedeCiclo1Proiezioni extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alunniSedeCiclo1Proiezioni', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('idAnnoProiezione');
            $table->char('codiceIs', 10)->index();
            $table->char('codicePe', 10)->index();
            $table->integer('alunni')->nullable()->default(0);

            $table->foreign('idAnnoProiezione', 'fk_alunniSedeCiclo1Proiezioni_anni')
                ->references('id')->on('anniScolasticiProiezioni')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');
            $table->unique(['idAnnoProiezione', 'codicePe']);


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alunniSedeCiclo1Proiezioni');
    }
}
