<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCPIAPuntiErogazioneLivello1HasCPITipiPercorsoLivello1Table extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CPIAPuntiErogazioneLivello1HasCPITipiPercorsoLivello1', function (Blueprint $table) {
            $table->integer('CPIAPuntiErogazioneLivello1_id');
            $table->integer('CPIATipiPercorsoLivello1_id');
            $table->integer('nPattiFormativi')->nullable();
            $table->integer('nDiversamenteAbili')->nullable();
            $table->primary(['CPIAPuntiErogazioneLivello1_id', 'CPIATipiPercorsoLivello1_id'], 'CPIAPuntiErogazioneLivello1HasCPITipiPercorsoLivello1_pkey');
            $table->foreign('CPIAPuntiErogazioneLivello1_id', 'fk_CPIAPuntiErogazioneLivello1_has_CPITipiPercorsoLivello1CP1')
                ->references('id')->on('CPIAPuntiErogazioneLivello1')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');
            $table->foreign('CPIATipiPercorsoLivello1_id', 'fk_CPIAPuntiErogazioneLivello1_has_CPITipiPercorsoLivello1CP2')
                ->references('id')->on('CPIATipiPercorsoLivello1')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('CPIAPuntiErogazioneLivello1HasCPITipiPercorsoLivello1');
    }

}
