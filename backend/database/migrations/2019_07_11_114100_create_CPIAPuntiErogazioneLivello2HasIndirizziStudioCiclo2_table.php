<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCPIAPuntiErogazioneLivello2HasIndirizziStudioCiclo2Table extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CPIAPuntiErogazioneLivello2HasIndirizziStudioCiclo2', function (Blueprint $table) {
            $table->integer('CPIAPuntiErogazioneLivello2_id', false, true);
            $table->string('indirizziStudioCiclo2_codice', 5);
            $table->integer('nPattiFormativi')->nullable();
            $table->integer('nDiversamenteAbili')->nullable();
            $table->primary(['CPIAPuntiErogazioneLivello2_id', 'indirizziStudioCiclo2_codice'], 'CPIAPuntiErogazioneLivello2HasIndirizziStudioCiclo2_pkey');
            $table->foreign('CPIAPuntiErogazioneLivello2_id', 'fk_CPIAPuntiErogazione2LivelloHas_indirizziStudio2Ciclo_CPIA1')
                ->references('id')->on('CPIAPuntiErogazioneLivello2')
                ->onUpdate('RESTRICT')
                ->onDelete('RESTRICT');
            $table->foreign('indirizziStudioCiclo2_codice', 'fk_CPIAPuntiErogazione2LivelloHas_indirizziStudio2Ciclo_indi1')
                ->references('codice')->on('indirizziStudioCiclo2')
                ->onUpdate('RESTRICT')
                ->onDelete('RESTRICT');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('CPIAPuntiErogazioneLivello2HasIndirizziStudioCiclo2');
    }

}
