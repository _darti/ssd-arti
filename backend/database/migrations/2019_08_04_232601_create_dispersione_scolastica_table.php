<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDispersioneScolasticaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dispersioneScolastica', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codiceCatastaleComune', '4');
            $table->integer('idAnnoScolastico');
            $table->string('ciclo', 10);
            $table->integer('iscritti')->nullable();
            $table->integer('abbandoni')->nullable();
            $table->integer('evasioni')->nullable();
            $table->integer('ripetenze')->nullable();
            $table->integer('frequenzeIrregolari')->nullable();
            $table->unique(['codiceCatastaleComune', 'idAnnoScolastico', 'ciclo']);
            $table->foreign('idAnnoScolastico', 'fk_dispersioneScolastica_idAnnoScolastico')
                ->references('id')->on('anniScolastici')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
            $table->foreign('codiceCatastaleComune', 'fk_dispersioneScolastica_codiceCatastaleComune')
                ->references('codiceCatastale')->on('comuni')
                ->onUpdate('CASCADE')
                ->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dispersioneScolastica');
    }
}
