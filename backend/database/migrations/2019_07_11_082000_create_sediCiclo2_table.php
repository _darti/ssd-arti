<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSediCiclo2Table extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sediCiclo2', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codice', 10);
            $table->bigInteger('idIstituzioneScolastica');
            $table->string('codiceCatastaleComune', 4)->nullable();
            $table->string('comune', 100);
            $table->string('indirizzo', 100)->nullable();
            $table->string('denominazione', 45)->nullable();
            $table->string('tipo', 45)->nullable();
            $table->boolean('sedeDirezioneAmministrativa')->default(0)->index('idx_direzioni_amministrativeCiclo2');
            $table->string('verticalizzazione', 45)->nullable();
            $table->string('tipologiaScuola', 2)->index('fk_tipologiScuola2_idx');
            $table->string('codiceEdificio', 45);
            $table->geometry('coordinate')->nullable();
            $table->foreign('tipologiaScuola', 'fk_sediCiclo2_tipologiaScuola')
                ->references('tipologiaScuola')->on('tipologieScuola')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');
            $table->foreign('idIstituzioneScolastica', 'fk_sediCiclo2_codiceIstituzione')
                ->references('id')->on('istituzioniScolastiche')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');
            $table->foreign('codiceCatastaleComune', 'fk_sediCiclo2_codiceCatastaleComune')
                ->references('codiceCatastale')->on('comuni')
                ->onUpdate('CASCADE')
                ->onDelete('SET NULL');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sediCiclo2');
    }

}
