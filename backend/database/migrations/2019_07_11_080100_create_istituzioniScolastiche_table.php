<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIstituzioniScolasticheTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('istituzioniScolastiche', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('idAnnoScolastico', false, true);
            $table->string('codice', 10);
            $table->string('codiceAmbito', 4)->nullable()->index('fk_istituzioniScolastiche_3_idx');
            $table->integer('tipoIstituto')->nullable()->index('fk_istituzioniScolastiche_2_idx');
            $table->string('denominazione', 100);
            $table->string('comuneSedeDirigenza', 100)->nullable();
            $table->string('codiceCatastaleComuneDirigenza', 4)->nullable();
            $table->unique(['idAnnoScolastico', 'codice'], 'idx_is_idAnnoScolastico_unique');
            $table->foreign('idAnnoScolastico', 'fk_idAnnoScolastico')
                ->references('id')->on('anniScolastici')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
            $table->foreign('tipoIstituto', 'fk_tipoIstituto')
                ->references('id')->on('tipiIstituto')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
            $table->foreign('codiceAmbito', 'fk_codiceAmbito')
                ->references('codice')->on('ambitiTerritoriali')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
            $table->foreign('codiceCatastaleComuneDirigenza', 'fk_sediCiclo1_codiceCatastaleComune')
                ->references('codiceCatastale')->on('comuni')
                ->onUpdate('CASCADE')
                ->onDelete('SET NULL');

        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('istituzioniScolastiche');
    }

}
