/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {Injectable} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IstituzioneScolastica} from '../../models/IstituzioneScolastica';
import {AnnoScolastico} from '../../models/AnnoScolastico';


@Injectable({
    providedIn: 'root'
})
export class ISService {

    private url = environment.apiUrl + '/is/';

    constructor(private http: HttpClient) {

    }

    public list(idAnno: number): Observable<Array<IstituzioneScolastica>> {
        return this.http.get<Array<IstituzioneScolastica>>(this.url + 'list?idAnno=' + idAnno);
    }

    public cerca(codiceIS: string, idAnno: number): Observable<IstituzioneScolastica> {
        return this.http.get<IstituzioneScolastica>(this.url + 'cerca/' + codiceIS + '?idAnno=' + idAnno);
    }

    public ciclo1(year: AnnoScolastico): Observable<Array<IstituzioneScolastica>> {
        return this.http.get<Array<IstituzioneScolastica>>(this.url + 'ciclo1/?idAnno=' + year.id);
    }


    public ciclo2(year: AnnoScolastico): Observable<Array<IstituzioneScolastica>> {
        return this.http.get<Array<IstituzioneScolastica>>(this.url + 'ciclo2/?idAnno=' + year.id);
    }


    public popolazione3AnniPrecedenti(annoScolastico: AnnoScolastico): Observable<Array<IstituzioneScolastica>> {
        return this.http.get<Array<IstituzioneScolastica>>(this.url + 'popolazione3anniprecedenti/?idAnno=' + annoScolastico.id);
    }

    public sovraDimensionate(year: AnnoScolastico): Observable<any> {
        return this.http.get(this.url + 'sovradimensionate/?idAnno=' + year.id);
    }

    public sottoDimensionate(year: AnnoScolastico): Observable<any> {
        return this.http.get(this.url + 'sottodimensionate/?idAnno=' + year.id);
    }

    public odof(year: AnnoScolastico): Observable<{ data, header }> {
        return this.http.get<{ data, header }>(this.url + 'odof/?idAnno=' + year.id);
    }

}
