/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {Injectable} from '@angular/core';
import {fromLonLat} from 'ol/proj';
import Feature from 'ol/Feature';
import VectorLayer from 'ol/layer/Vector';
import {Vector} from 'ol/source';
import {BuildMapService} from '../../../maps/mappa-puglia/build-map/build-map.service';
import {PuntiUnivoci} from '../../helper/punti-univoci';
import {Edificio} from '../../models/Edificio';
import Point from 'ol/geom/Point';


const PREFISSO_EDIFICIO = 'ED_';

@Injectable({
    providedIn: 'root'
})
export class EdificiUtilsService {


    private pointsUtility = new PuntiUnivoci();

    constructor(private  buildMapsService: BuildMapService) {
    }


    public buildLayerEdificiInutilizzati(edifici: Edificio[]) {

        const features = [];
        edifici.forEach((edificio) => {
            if (edificio.coordinate) {
                const coordinates = this.pointsUtility.assicuraPuntoUnivoco(edificio.coordinate.coordinates);
                const p = new Point(fromLonLat(coordinates));
                const f = new Feature({geometry: p});
                f.setId(PREFISSO_EDIFICIO + edificio.id);
                f.setStyle((feature, resolution) => {
                    const displayText = resolution <= this.buildMapsService.getMinResolutionForText();
                    let label: String = '';
                    if (displayText) {
                        label = edificio.codiceEdificio;
                    }
                    const visualizza = displayText;
                    return this.buildMapsService.getStyleEdificio(feature, displayText, label, visualizza);
                });


                features.push(f);


            }
        });


        return new VectorLayer({
            source: new Vector({
                features: features
            }),
            zIndex: 4,
        });


    }


}
