/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {toDegrees, toRadians} from 'ol/math';
import {DEFAULT_RADIUS, getDistance} from 'ol/sphere';

const USA_OFFSET_RANDOM = false;


export const distanzaMinimaMeters = 10;

export class PuntiUnivoci {

    private pointsUnique: Array<number[]> = [];

    private lastBeaming = 0;

    public resetPunti() {
        this.pointsUnique = [];
    }

    public assicuraPuntoUnivoco(coordinates: number[]): number[] {
        if (this.pointsUnique.length === 0) {
            this.pointsUnique.push(coordinates);
            return coordinates;
        }
        const distances: Array<number> = this.pointsUnique.map(p => getDistance(coordinates, p));
        const nearestDistance = Math.min(...distances);

        if (nearestDistance < distanzaMinimaMeters) {
            // console.warn('punto troppo vicino ' + nearestDistance, coordinates);
            let newCoordinates: number[];
            if (USA_OFFSET_RANDOM) {
                newCoordinates = this.randomOffset(coordinates);
            } else {
                newCoordinates = this.offset(coordinates, 40, Math.round(Math.PI / 12 * this.lastBeaming++));
            }
            return this.assicuraPuntoUnivoco(newCoordinates);
        } else {
            this.pointsUnique.push(coordinates);
            return coordinates;
        }

    }

    /**
     * Returns the coordinate at the given distance and bearing from `c1`.
     *
     * @param {Coordinate} c1 The origin point (`[lon, lat]` in degrees).
     * @param {number} distance The great-circle distance between the origin
     *     point and the target point.
     * @param {number} bearing The bearing (in radians).
     * @return {Coordinate} The target point.
     */
    private offset(c1, distance, bearing) {
        const lat1 = toRadians(c1[1]);
        const lon1 = toRadians(c1[0]);
        const dByR = distance / DEFAULT_RADIUS;
        const lat = Math.asin(
            Math.sin(lat1) * Math.cos(dByR) +
            Math.cos(lat1) * Math.sin(dByR) * Math.cos(bearing)
        );
        const lon = lon1 + Math.atan2(
            Math.sin(bearing) * Math.sin(dByR) * Math.cos(lat1),
            Math.cos(dByR) - Math.sin(lat1) * Math.sin(lat)
        );
        return [toDegrees(lon), toDegrees(lat)];
    }

    private randomOffset(aPoint: number[]): number[] {
        const newCoordinates: number[] = [];
        const t = Math.random();
        const e = 0.0005;
        const x = Math.cos(t) * e;
        const y = Math.sin(t) * e;
        // console.log(x, y);
        newCoordinates[0] = aPoint[0] + x;
        newCoordinates[1] = aPoint[1] + y;
        return newCoordinates;
    }
}
