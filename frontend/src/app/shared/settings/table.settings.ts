/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

export const Table = {
    sede: [
        {name: 'Codice', prop: 'idSede'},

        {name: 'Codice catastale ', prop: 'codiceCatastaleComune'},

        {
            name: 'Codice edificio',
            prop: 'codiceEdificio'
        },

        {
            name: 'Codice istituzione',
            prop: 'codiceIstituzione'
        },

        {
            name: 'Comune',
            prop: 'comune'
        },

        {
            name: 'Denominazione',
            prop: 'denominazione'
        },

        {
            name: 'Indirizzo',
            prop: 'indirizzo'
        },

        {
            name: 'Direzione amministrativa',
            prop: 'sedeDirezioneAmministrativa'
        },

        {
            name: 'Tipo',
            prop: 'tipo'
        },

        {
            name: 'Tipologia scuola',
            prop: 'tipologiaScuola'
        }
    ]
};

export const MapsTable = {

    popolazioneScolastica14_18: [
        {prop: 'fascia_3_13_0_14_18', name: 'Fascia 3-13 o 14-18'},
        {prop: 'provincia', name: 'Provincia'},
        {prop: 'comune', name: 'Comune'},
        {prop: 'popolazione2013', name: 'Popolazione al 2013'},
        {prop: 'popolazione2017', name: 'Popolazione al 2017'},
        {prop: 'variazioneMediaAnnua2013-2017', name: 'Variazione media annua 2013-2017'},
        {prop: 'popolazionePrevista2022', name: 'Popolazione prevista per il 2022'},
        {prop: 'variazioneMediaAnnuaPrevista2022-2018', name: 'Variazione media annua prevista 2022-2018'}
    ],
    popolastica3AnniPrecedenti: [

        {prop: 'ProvinciaPE', name: 'ProvinciaPE'},
        {prop: 'ComunePE', name: 'CodicePE'},
        {prop: 'DenominazioneIS', name: ''},
        {prop: 'CodicePE', name: 'CodicePE'},
        {prop: 'DenominazionePE', name: 'DenominazionePE'},
        {prop: 'totale alunni IS 2019/2020', name: 'totale alunni IS 2019/2020'},
        {prop: 'totale alunni PE 2019/2020', name: 'totale alunni PE 2019/2020'}

    ],
    modificazioniIntervenute: [
        {prop: 'provincia', name: 'Provincia'},
        {prop: 'comune', name: 'Comune'},
        {prop: 'denominazioneIS', name: 'Denominazione IS'},
        {prop: 'modificazioni2016_2017', name: 'Modificazioni 2016-2017'},
        {prop: 'modificazioni2017_2018', name: 'Modificazioni 2017-2018'},
        {prop: 'modificazioni2018_2019', name: 'Modificazioni 2018-2019'},
        {prop: 'modificazioni2019_2020', name: 'Modificazioni 2019-2020'}
    ]


};



