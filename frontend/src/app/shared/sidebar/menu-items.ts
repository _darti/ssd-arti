/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {RouteInfo} from './sidebar.metadata';

export const ROUTES: RouteInfo[] = [
    // {
    //     path: '',
    //     title: 'Principali',
    //     icon: '',
    //     class: 'nav-small-cap',
    //     label: '',
    //     labelClass: '',
    //     showAsHeader: true,
    //     submenu: []
    // },
    {
        path: '',
        title: 'Rete scolastica',
        icon: 'mdi mdi-google-maps',
        class: 'openSubMenu',
        label: '',
        labelClass: '',
        showAsHeader: false,
        submenu: [
            {
                path: '/pages/assettoIstituzioniScolastiche',
                title: 'Assetto Istituzioni Scolastiche',
                icon: '',
                class: '',
                label: '',
                labelClass: '',
                showAsHeader: false,
                submenu: []
            },
            {
                path: '/pages/offertaFormativaIICiclo',
                title: 'Offerta formativa del II ciclo ',
                icon: '',
                class: '',
                label: '',
                labelClass: '',
                showAsHeader: false,
                submenu: []
            },
            {
                path: '/pages/dashboard/cpia',
                title: 'CPIA',
                icon: '',
                class: '',
                label: '',
                labelClass: '',
                showAsHeader: false,
                submenu: []
            }
        ]
    },
    {
        path: '',
        title: 'Basi di dati',
        icon: 'mdi mdi-table-large',
        class: 'openSubMenu',
        label: '',
        labelClass: '',
        showAsHeader: false,
        submenu: [
            {
                path: '/pages/dashboard/organiciIs',
                title: 'Organici Istituzioni Scolastiche',
                icon: '',
                class: '',
                label: '',
                labelClass: '',
                showAsHeader: false,
                submenu: []
            },
            {
                path: '/pages/dashboard/demografiaComunale',
                title: 'Demografia comunale',
                icon: '',
                class: '',
                label: '',
                labelClass: '',
                showAsHeader: false,
                submenu: []
            },
            {
                path: '/pages/dashboard/andamentoIscrizioni',
                title: 'Andamento Iscrizioni',
                icon: '',
                class: '',
                label: '',
                labelClass: '',
                showAsHeader: false,
                submenu: []
            },
            {
                path: '/pages/dashboard/baciniUtenza',
                title: 'Bacini di utenza indirizzi di studio',
                icon: '',
                class: '',
                label: '',
                labelClass: '',
                showAsHeader: false,
                submenu: []
            },

            {
                path: '/pages/dashboard/provenienze',
                title: 'Provenienze studenti II ciclo',
                icon: '',
                class: '',
                label: '',
                labelClass: '',
                showAsHeader: false,
                submenu: []
            },

            {
                path: '/pages/dashboard/dispersione',
                title: 'Dispersione scolastica',
                icon: '',
                class: '',
                label: '',
                labelClass: '',
                showAsHeader: false,
                submenu: []
            },
            // {
            //     path: '/pages/dashboard/popolazione-anni',
            //     title: 'Popolazione anni precedenti',
            //     icon: '',
            //     class: '',
            //     label: '',
            //     labelClass: '',
            //     showAsHeader: false,
            //     submenu: []
            // },
            // {
            //       path: '/pages/dashboard/popolazione-scolastica',
            //       title: 'Popolazione scolastica',
            //       icon: '',
            //       class: '',
            //       label: '',
            //       labelClass: '',
            //       showAsHeader: false,
            //       submenu: []
            //   },
            {
                path: '/pages/dashboard/storiaDimensionamento',
                title: 'Storia Dimensionamento',
                icon: '',
                class: '',
                label: '',
                labelClass: '',
                showAsHeader: false,
                submenu: []
            },
            {
                path: '/pages/dashboard/disagioEconomicoSociale',
                title: 'Disagio economico/sociale',
                icon: '',
                class: '',
                label: '',
                labelClass: '',
                showAsHeader: false,
                submenu: []
            },
        ]
    },
    {
        path: '',
        title: 'Documentazione amministrativa 2020/21',
        icon: 'mdi mdi-file-document-box',
        class: 'openSubMenu',
        label: '',
        labelClass: '',
        showAsHeader: false,
        submenu: [
            {
                title: 'Linee guida regionali',
                path: '/documentazione2021/lineeGuida',
                icon: '',
                class: '',
                label: '',
                labelClass: '',
                showAsHeader: false,
                submenu: []
            },
            {
                title: 'Documenti utili',
                path: '/documentazione2021/documentiUtili',
                icon: '',
                class: '',
                label: '',
                labelClass: '',
                showAsHeader: false,
                submenu: []
            },
            {
                title: 'Richieste, pareri, piani',
                path: '/documentazione2021/richiestePareriPiani',
                icon: '',
                class: '',
                label: '',
                labelClass: '',
                showAsHeader: false,
                submenu: []
            },
        ]
    },
];
