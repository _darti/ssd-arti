/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {AnnoScolastico} from '../../../../shared/models/AnnoScolastico';
import {Subscription} from 'rxjs';
import {Comune} from '../../../../shared/models/Comune';
import {ComuniService} from '../../../comuni/comuni.service';
import {AnnoScolasticoService} from '../../../anno-scolastico/anno-scolastico.service';
import {ISService} from '../../../../shared/services/istituzioniScolastiche/is.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {FormatRowTableService} from '../../../../shared/services/format-row-table.service';
import {ExportToolsService} from '../../../../shared/utils/export-tools.service';
import * as _ from 'lodash';


export interface Denominazione {
    comuneIS: string,
    denominazioneIS: string


}

export interface Ciclo {

}

export interface Tipo {

}


@Component({
    selector: 'app-organico-di-diritto-di-fatto',
    templateUrl: './organico-di-diritto-di-fatto.component.html',
    styleUrls: ['./organico-di-diritto-di-fatto.component.css']
})
export class OrganicoDiDirittoDiFattoComponent implements OnInit, OnDestroy {


    @Input()
    public showDashboard: boolean;


    @ViewChild(DatatableComponent) table: DatatableComponent;
    private saveAllRows: Array<any>;
    annoScolastico: AnnoScolastico;
    private annoScolasticoSubscription: Subscription;
    public rows: any[];
    public columns;
    public ambiti: Array<any>;
    public province: string[];
    public provinciaSelected = 'Tutti';
    public comuneSelected = 'Tutti';
    public tipoSelected = 'Tutti';
    public cicloSelected = 'Tutti';
    public cicli: string[];

    public tipologie: Tipo[];

    public comuni: Comune[];
    private saveComuniTot: Comune[];
    public denominazioneSelected = 'Tutti';
    public denominazioni: Denominazione[] = null;
    public saveDenominazioniTot: Denominazione[] = null;
    public title = 'Organico  Diritto/Fatto ';


    constructor(
        private comuniService: ComuniService,
        private annoScolasticoService: AnnoScolasticoService,
        private isService: ISService,
        private spinner: NgxSpinnerService,
        private formatRowService: FormatRowTableService,
        private exportUtilsService: ExportToolsService
    ) {

        this.columns = null;
    }


    public getRowClass = row => {
        /*
        if (row.cod_ambito === this.selectAmbito) {
          return {
            'row-color': true
          };
        } else {

        }
        */

    };

    public exportCsv() {
        let columnsHeadersForExport = [];
        this.columns.forEach(item => columnsHeadersForExport.push(item.name));
        this.exportUtilsService.exportTableData(this.title + this.annoScolastico.label,
            this.saveAllRows, columnsHeadersForExport);

    }


    ngOnInit() {
        this.annoScolasticoService.getAnnoScolasticoObservable().subscribe(annoScolastico => {

            if (annoScolastico) {
                this.spinner.show();
                this.resetFiltri();
                this.annoScolastico = annoScolastico;
                this.loadDati();

            }

        });


    }

    private resetFiltri() {
        this.comuneSelected = 'Tutti';
        this.cicloSelected = 'Tutti';
        this.denominazioneSelected = 'Tutti';
        this.tipoSelected = 'Tutti';
    }


    private loadDati() {
        this.isService.odof(this.annoScolastico).subscribe(data => {
            this.columns = data.header;
            this.rows = data.data;
            this.saveAllRows = this.rows;
            this.loadFilters();
            this.spinner.hide();
        });


    }

    private loadFilters() {
        this.cicli = (_.uniq(this.saveAllRows.map(item => item.ciclo))).sort();
        this.province = (_.uniq(this.saveAllRows.map(item => item.provincia))).sort();
        this.comuni = _.sortBy(_.uniqBy(this.saveAllRows.map((item) => {
            return {'provincia': item.provincia, 'nome': item.comuneIS};
        }), 'nome'), 'nome');
        this.saveComuniTot = this.comuni;
        this.denominazioni = _.sortBy(_.uniqBy(this.saveAllRows.map(item => {
            return {
                comuneIS: item.comuneIS, denominazioneIS: item.denominazioneIS
            };
        }), 'denominazioneIS'), 'denominazioneIS');
        this.saveDenominazioniTot = this.denominazioni;
        this.tipologie = (_.uniq(this.saveAllRows.map(item => item.tipologiaScuola))).sort();
    }


    public filter() {
        this.spinner.show();
        this.rows = this.saveAllRows;
        this.filterByCiclo();
        this.filterByProvincia();
        this.filterByComune();
        this.filterByDenominazione();
        this.filterByTipologia();
        this.sortRowsByCiclo();
        this.spinner.hide();
    }


    public filterByCiclo() {
        if (this.cicloSelected !== 'Tutti') {
            this.rows = this.rows.filter(
                item => item.ciclo.toLowerCase() === this.cicloSelected.toLowerCase()
            );
        }
    }


    public filterByProvincia() {
        if (this.provinciaSelected !== 'Tutti') {
            this.rows = this.rows.filter(
                item => item.provincia.toLowerCase() === this.provinciaSelected.toLowerCase()
            );
            this.comuni = this.saveComuniTot.filter(item => item.provincia.toLowerCase() === this.provinciaSelected.toLowerCase());
        } else {
            this.comuneSelected = 'Tutti';
            this.comuni = this.saveComuniTot;

        }
    }


    private filterByComune() {
        if (this.comuneSelected !== 'Tutti') {
            this.rows = this.rows.filter(
                item => item.comuneIS.toLowerCase() === this.comuneSelected.toLowerCase()
            );
            this.denominazioni = this.saveDenominazioniTot.filter(item => item.comuneIS.toLowerCase() === this.comuneSelected.toLowerCase());

        } else {
            this.denominazioni = this.saveDenominazioniTot;
            this.denominazioneSelected = 'Tutti';
        }

    }

    public filterByDenominazione() {
        if (this.denominazioneSelected !== 'Tutti') {
            this.rows = this.rows.filter(
                item => item.denominazioneIS.toLowerCase() === this.denominazioneSelected.toLowerCase()
            );
        }
    }


    public filterByTipologia() {
        if (this.tipoSelected !== 'Tutti') {
            this.rows = this.rows.filter(
                item => item.tipologiaScuola.toLowerCase() === this.tipoSelected.toLowerCase()
            );
        }
    }


    private sortRowsByCiclo() {
        this.rows = _.sortBy(this.rows, 'ciclo');
    }

    ngOnDestroy() {
        if (this.annoScolasticoSubscription) {
            this.annoScolasticoSubscription.unsubscribe();
        }
    }


    ngAfterViewInit() {
        //   this.spinner.show();
    }

}
