/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {Component, Input, OnInit} from '@angular/core';
import {Comune} from '../../../../../shared/models/Comune';
import {Provincia} from '../../../../../shared/models/Provincia';
import {ExportToolsService} from '../../../../../shared/utils/export-tools.service';
import {NgxSpinnerService} from 'ngx-spinner';
import * as _ from 'lodash';
import {AnnoScolastico} from '../../../../../shared/models/AnnoScolastico';

export interface DenominazioniCpia {
    denominazione: string;
    comune: string;
}


@Component({
    selector: 'app-cpia-general-data-table',
    templateUrl: './cpia-general-data-table.component.html',
    styleUrls: ['./cpia-general-data-table.component.css']
})
export class CpiaGeneralDataTableComponent implements OnInit {


    @Input()
    public columns;

    @Input()
    public rows;

    @Input()
    public annoScolastico: AnnoScolastico;

    public title = 'CPIA';

    public saveAllRows: Array<any>;

    public comuni: Comune[];
    private saveComuniTot: Comune[];

    public comuneSelected = 'Tutti';
    public province: Provincia[];
    public provinciaSelected = 'Tutti';
    public saveDenominazioniTot: DenominazioniCpia[];
    public denominazioni: DenominazioniCpia[];
    public denominazioneSelected = 'Tutti';


    constructor(private spinner: NgxSpinnerService, private exportUtilsService: ExportToolsService) {
    }


    ngOnInit() {
        console.log('riga', this.rows);
        this.saveAllRows = this.rows;
        this.loadFilters();
    }

    private resetFiltri() {
        this.comuneSelected = 'Tutti';
        this.denominazioneSelected = 'Tutti';

    }


    private loadFilters() {


        this.province = (_.uniq(this.saveAllRows.map(item => item.provincia))).sort();
        this.comuni = _.sortBy(_.uniqBy(this.saveAllRows.map((item) => {
            return {'provincia': item.provincia, 'nome': item.comune};
        }), 'nome'), 'nome');
        this.saveComuniTot = this.comuni;
        this.denominazioni = _.sortBy(_.uniqBy(this.saveAllRows.map(item => {
            return {
                comune: item.comune, denominazione: item.denominazione
            };
        }), 'denominazione'), 'denominazione');
        this.saveDenominazioniTot = this.denominazioni;
    }


    public filter() {
        this.spinner.show();
        this.rows = this.saveAllRows;
        this.filterByProvincia();
        this.filterByComune();
        this.filterByDenominazione();
        this.spinner.hide();
    }

    public filterByProvincia() {
        if (this.provinciaSelected !== 'Tutti') {
            this.rows = this.rows.filter(
                item => item.provincia.toLowerCase() === this.provinciaSelected.toLowerCase()
            );
            this.comuni = this.saveComuniTot.filter(item => item.provincia.toLowerCase() === this.provinciaSelected.toLowerCase());
        } else {
            this.comuneSelected = 'Tutti';
            this.comuni = this.saveComuniTot;

        }
    }


    private filterByComune() {

        if (this.comuneSelected !== 'Tutti') {
            this.rows = this.rows.filter(
                item => item.comune.toLowerCase() === this.comuneSelected.toLowerCase()
            );
            this.denominazioni = this.saveDenominazioniTot.filter(item => item.comune.toLowerCase() === this.comuneSelected.toLowerCase());

        } else {
            this.denominazioni = this.saveDenominazioniTot;
            this.denominazioneSelected = 'Tutti';
        }
    }

    public filterByDenominazione() {
        if (this.denominazioneSelected !== 'Tutti') {
            this.rows = this.rows.filter(
                item => item.denominazione.toLowerCase() === this.denominazioneSelected.toLowerCase()
            );
        }
    }

    public exportCsv() {
        let columnsHeadersForExport = [];
        this.columns.forEach(item => columnsHeadersForExport.push(item.name));
        this.exportUtilsService.exportTableData(this.title + this.annoScolastico.label,
            this.saveAllRows, columnsHeadersForExport);
    }
}
