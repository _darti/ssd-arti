/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {Component, OnInit} from '@angular/core';
import {AnnoScolastico} from '../../../../../shared/models/AnnoScolastico';
import {DemografieService} from '../demografie.service';
import {AnnoScolasticoService} from '../../../../anno-scolastico/anno-scolastico.service';

@Component({
    selector: 'app-trend-demografici',
    templateUrl: './trend-demografici.component.html',
    styleUrls: ['./trend-demografici.component.css']
})
export class TrendDemograficiComponent implements OnInit {

    subtitle: string;
    public annoScolastico: AnnoScolastico;
    // lineChart
    public lineChartData1: Array<any> = [{data: [], label: ''}];

    // da carica con gli anni da visualizzare
    public lineChartLabels1: Array<any> = [];

    public constructor(private trendService: DemografieService, private  annoScolasticoService: AnnoScolasticoService) {

    }


    public lineChartOptions1: any = {
        scales: {
            yAxes: [
                {
                    ticks: {
                        beginAtZero: true
                    },
                    gridLines: {
                        color: 'rgba(0, 0, 0, 0.1)'
                    }
                }
            ],
            xAxes: [
                {
                    gridLines: {
                        color: 'rgba(0, 0, 0, 0.1)'
                    }
                }
            ]
        },
        lineTension: 10,
        responsive: true,
        maintainAspectRatio: false,
        elements: {line: {tension: 0}}
    };


    public lineChartColors1: Array<any> = [
        {
            // grey
            backgroundColor: 'rgba(6,215,156,0.1)',
            borderColor: 'rgba(6,215,156,1)',
            pointBackgroundColor: 'rgba(6,215,156,1)',
            pointBorderColor: '#fff',

            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(6,215,156,0.5)'
        },
        {
            // dark grey
            backgroundColor: 'rgba(57,139,247,0.1)',
            borderColor: 'rgba(57,139,247,1)',
            pointBackgroundColor: 'rgba(57,139,247,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(57,139,247,0.5)'
        }
    ];
    public lineChartLegend1 = false;
    public lineChartType1 = 'line';

    public ngOnInit(): void {
        this.annoScolasticoService.getAnnoScolasticoObservable().subscribe(annoScolastico => {

            this.lineChartData1 = [{data: [], label: ''}];
            this.lineChartLabels1 = [];
            if (annoScolastico) {
                this.annoScolastico = annoScolastico;
                this.trendService.trend(this.annoScolastico).subscribe(resp => {
                    console.log(resp);


                    Object.keys(resp.popolazione).forEach(item => this.lineChartLabels1.push(item));

                    const data1 = {
                        data: [],
                        label: Object.keys(resp)[0]

                    };
                    Object.keys(resp.popolazione).forEach(elem => {
                        data1.data.push(resp.popolazione[elem]);
                    });

                    this.lineChartData1.pop();
                    this.lineChartData1.push(data1);

                    const data2 = {
                        data: [],
                        label: Object.keys(resp)[0]

                    };
                    Object.keys(resp.popolazioneEtaScolare).forEach(elem => {
                        data2.data.push(resp.popolazioneEtaScolare[elem]);
                    });
                    this.lineChartData1.push(data2);


                    console.log(this.lineChartData1);


                });
            }

        });


    }


}

