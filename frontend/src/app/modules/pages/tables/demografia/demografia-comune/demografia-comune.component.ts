/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {Component, Input, OnInit} from '@angular/core';
import {DemografiaComunale} from '../demografia-comunale/demografiaComunale';

@Component({
    selector: 'app-demografia-comune',
    template: '<div><p-chart type="line" [data]="data"></p-chart></div>'
})
export class DemografiaComuneComponent implements OnInit {

    _demografiaComune: DemografiaComunale;
    data = {};


    @Input()
    set demografiaComune(value: DemografiaComunale) {
        this._demografiaComune = value;
        if (!value) {
            return;
        }
        this.data = {
            labels: ['2015', '2019', 'Previsione 2023'],
            datasets: [
                {
                    label: '3-13 anni',
                    backgroundColor: 'rgba(54, 162, 235, 0.2)',
                    borderColor: 'rgba(54, 162, 235, 1)',
                    fill: false,
                    data: [
                        this._demografiaComune.pop_3_13_2015,
                        this._demografiaComune.pop_3_13_2019,
                        this._demografiaComune.prev_3_13_2023,
                    ]
                },
                {
                    label: '3-5 anni',
                    backgroundColor: 'rgba(75, 192, 192, 0.2)',
                    borderColor: 'rgba(75, 192, 192, 1)',
                    fill: false,

                    data: [
                        this._demografiaComune.pop_3_5_2015,
                        this._demografiaComune.pop_3_5_2019,
                        this._demografiaComune.prev_3_5_2023,
                    ]
                },
                {
                    label: '6-10 anni',
                    backgroundColor: 'rgba(255, 99, 132, 0.2)',
                    borderColor: 'rgba(255, 99, 132, 1)',
                    fill: false,

                    data: [
                        this._demografiaComune.pop_6_10_2015,
                        this._demografiaComune.pop_6_10_2019,
                        this._demografiaComune.prev_6_10_2023,
                    ]
                },
                {
                    label: '11-13 anni',
                    // backgroundColor: 'rgba(255, 206, 86, 0.2)',
                    borderColor: 'rgba(255, 206, 86, 1)',
                    fill: false,

                    data: [
                        this._demografiaComune.pop_11_13_2015,
                        this._demografiaComune.pop_11_13_2019,
                        this._demografiaComune.prev_11_13_2023,
                    ]
                },
                {
                    label: '14-18 anni',
                    backgroundColor: 'rgba(255, 159, 64, 0.2)',
                    borderColor: 'rgba(255, 159, 64, 1)',
                    fill: false,

                    data: [
                        this._demografiaComune.pop_14_18_2015,
                        this._demografiaComune.pop_14_18_2019,
                        this._demografiaComune.prev_14_18_2023,
                    ]
                }
            ],
            options: {
                responsive: true,
                maintainAspectRatio: false
            }
        };
    }

    constructor() {
    }

    ngOnInit() {
    }


}
