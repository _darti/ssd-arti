/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {ComuniViciniComponent} from './comuni-vicini/comuni-vicini.component';
import {NgbTypeaheadModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule} from '@angular/forms';


const routes: Routes = [
    {
        path: '',
        data: {
            title: 'Comuni',
        },
        // component: PagesComponent,
        children: [
            {path: '', redirectTo: 'vicini', pathMatch: 'full'},

            {
                path: 'vicini',
                component: ComuniViciniComponent
            }
        ]
    },

];


@NgModule({
    declarations: [
        ComuniViciniComponent,
    ],
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        NgbTypeaheadModule,
        FormsModule,
    ]
})
export class ComuniModule {
}
