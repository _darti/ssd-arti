/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {Component, Input, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {SediCiclo2Service} from '../../shared/services/sediciclo2/sedi-ciclo2.service';
import {NgbTypeaheadConfig} from '@ng-bootstrap/ng-bootstrap';
import {SedeCiclo2} from '../../shared/models/SedeCiclo2';
import {IndirizzoDiStudio} from '../../shared/models/IndirizzoDiStudio';
import {MappaPugliaComponent} from '../mappa-puglia/mappa-puglia.component';
import Feature from 'ol/Feature.js';
import {Vector} from 'ol/source.js';
import VectorLayer from 'ol/layer/Vector.js';
import Point from 'ol/geom/Point';
import {BuildMapService} from '../mappa-puglia/build-map/build-map.service';
import {PuntiUnivoci} from '../../shared/helper/punti-univoci';
import {ActivatedRoute, Router} from '@angular/router';
import {Sede} from '../../shared/models/Sede';
import {AnnoScolasticoService} from '../../modules/anno-scolastico/anno-scolastico.service';
import {InfoSedi} from '../mappa-puglia/Utils';
import {FiltraSedi} from '../../shared/models/sedi/FiltraSedi';
import {Store} from '@ngrx/store';
import {IndirizziSelezionatiOffFormActions} from './offerta-formativa.actions';
import {AppState} from '../../reducers';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {TreeNode} from 'primeng/api';
import * as _ from 'lodash';
import GeoJSON from 'ol/format/GeoJSON';
import {Fill, Style} from 'ol/style';
import {AreaLegendField, LegendField, PointLegendField} from '../mappa-puglia/map-legenda/model';
import {TreeTable} from 'primeng/primeng';

class DettagliIndirizzo {
    constructor(public nSedi: number, public nAlunni: number) {
    }
}


@Component({
    selector: 'app-offerta-formativa-ciclo2',
    templateUrl: './offerta-formativa-ciclo2.component.html',
    styleUrls: ['./offerta-formativa-ciclo2.component.css']
})
export class OffertaFormativaCiclo2Component implements OnInit, OnDestroy {

    public selectedSede: SedeCiclo2;
    saveCoordinateClicked: any;
    public popupSedeSelezionata: SedeCiclo2;

    private filtraSedi: FiltraSedi = new FiltraSedi();
    indirizzi: Array<IndirizzoDiStudio>;
    indirizziRefactored: Array<TreeNode> = [];

    //  @ViewChild('popupTemplate') popup;
    sediConIndirizzo: Array<SedeCiclo2> = [];
    selectedRow: Array<SedeCiclo2>;


    @Input() popupTemplate: TemplateRef<any>;
    @ViewChild(DatatableComponent) table: DatatableComponent;
    // focus$ = new Subject<string>();
    // click$ = new Subject<string>();
    indirizziSelezionati: Array<IndirizzoDiStudio>;
    // searchModel: IndirizzoDiStudio = new IndirizzoDiStudio();
    showRisultati = false;
    // dettagliIndirizzo: DettagliIndirizzo;
    @ViewChild('dt') tabellaIndirizzi: TreeTable;
    @ViewChild('mappaPuglia') mappaPuglia: MappaPugliaComponent;
    private reteScolastica2Layer: Vector = null;
    private linkLayer2: Vector = null;
    private pointsUtility = new PuntiUnivoci();

    columns = [
        {prop: 'codice', name: 'CODICE'},
        {prop: 'comune', name: 'COMUNE'},
        {prop: 'denominazione', name: 'DENOMINAZIONE'},
    ];

    columnsIndirizzi = [
        {prop: 'tipo', name: 'TIPOLOGIA/CODICE', width: '25%'},
        {prop: 'denominazione', name: 'INDIRIZZO', width: '65%'},
        {prop: 'selected', name: '', width: '10%'},

        // {prop: 'tipologia', name: 'Tipologia'}
    ];

    legendFields: LegendField[] = [];


    public selectedNode1: Array<TreeNode>;
    public risultatoSelezionato: any;
    public showNav = true;
    public infoSedi = [InfoSedi.TUTTI, InfoSedi.NOMI, InfoSedi.OD_OF, InfoSedi.DISATTIVA];
    public infoSediSelected: string;
    private percorrenzeLayer: VectorLayer = null;
    private cachedStyles = {};

    private static getLayerColor(count: number): string {
        let color: string = null;
        if (count >= 5) {
            color = `rgba(48, 75, 115, 0.6)`;
        } else if (count === 4) {
            color = `rgba(48, 106, 158, 0.6)`;
        } else if (count === 3) {
            color = `rgba(31, 162, 203, 0.6)`;
        } else if (count === 2) {
            color = `rgba(154, 208, 219, 0.6)`;
        } else if (count === 1) {
            color = `rgba(218, 231, 240, 0.6)`;
        }
        return color;
    }

    constructor(
        private annoScolasticoService: AnnoScolasticoService,
        private sediCiclo2: SediCiclo2Service,
        private config: NgbTypeaheadConfig,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private buildMapsService: BuildMapService,
        private store: Store<AppState>
    ) {
        this.initLegend();
    }


    initLegend() {

        this.legendFields.push(new PointLegendField('P.E. II ciclo - Liceo', BuildMapService.getCircleColori2('li')));
        this.legendFields.push(new PointLegendField('P.E. II ciclo - Professionale', BuildMapService.getCircleColori2('ip')));
        this.legendFields.push(new PointLegendField('P.E. II ciclo - Tecnico', BuildMapService.getCircleColori2('it')));

        for (let i = 1; i <= 5; i++) {
            let label = '';
            if (i === 5) {
                label = '5+';
            } else {
                label = '' + i;
            }
            this.legendFields.push(new AreaLegendField(label, OffertaFormativaCiclo2Component.getLayerColor(i)));
        }
    }

    ngOnInit() {
        this.sediCiclo2.getIndirizziConFigli().subscribe((list) => {
            this.indirizzi = list;
            this.indirizziRefactored = this.indirizzi.map(item => {
                if (item.figli && item.figli.length > 0) {
                    item.figli = item.figli.map(elem => {
                        // console.info('indirizzo', elem);
                        return this.mapIndirizzoForTyping(elem, false);
                    });
                } else {
                    item.figli = [];
                }
                return this.mapIndirizzoForTyping(item, true);
            });
            this.store.subscribe((state) => {
                if (state.offFormativaState.selectedNode) {
                    this.selectedNode1 = state.offFormativaState.selectedNode;
                    this.selectedNode1.forEach(item => {
                        if (item.parent) {
                            this.indirizziRefactored.forEach((elem) => {
                                if (elem.data.codice === item.parent.data.codice) {
                                    elem.expanded = true;
                                    elem.partialSelected = true;
                                }
                            });
                        }
                    });
                } else {
                    this.selectedNode1 = [];
                }
                this.reloadIndirizziSelezionati();
                this.selectedRow = [];
                if (state.offFormativaState.sedeSelected) {
                    this.selectedSede = state.offFormativaState.sedeSelected;
                    this.mappaPuglia.coordinateClicked = state.offFormativaState.coordinateClicked;
                    this.risultatoSelezionato = this.selectedSede;
                    this.onSelectedPoint(this.selectedSede.codice);
                }
                this.aggiornaIndirizzi();
            });

        });
        this.config.showHint = true;

    }

    reloadIndirizziSelezionati(event?) {

        this.indirizziSelezionati = [];
        this.selectedNode1.forEach(item => this.indirizziSelezionati.push(item.data));
        this.aggiornaIndirizzi();
    }

    private mapIndirizzoForTyping(item, isParent?: boolean) {
        if (item.codice[0].trim() == 'L' && isParent) {

            item.denominazione = item.denominazione.replace('LICEO', '');
            return this.indirizzoLiceo(item);
        } else {
            return {
                'data': {'tipo': item.codice, 'codice': item.codice, 'denominazione': item.denominazione},
                'expanded': false,
                'children': item.figli
            };

        }

    }

    private indirizzoLiceo(item) {

        return {
            'data': {'tipo': 'LICEO', 'codice': item.codice, 'denominazione': item.denominazione},
            'expanded': false,
            'children': item.figli
        };

    }


    onSelectedPoint(codice) {
        this.saveCoordinateClicked = this.mappaPuglia.coordinateClicked;

        this.sediCiclo2.cerca(codice, this.annoScolasticoService.getAnnoScolastico().id).subscribe(resp => {
                this.selectedSede = resp[0];
                this.mappaPuglia.setSedePopup(this.selectedSede);
                this.risultatoSelezionato = this.selectedSede;
            }
        );
    }

    // carica gli indirizzi dalla tabella, devo cambiarlo
    selectIndirizziFromTable({selected}) {
        if (this.indirizziSelezionati.length > 0) {
            this.indirizziSelezionati.splice(0, this.indirizziSelezionati.length);
            this.indirizziSelezionati.push(...selected);
        }
        this.aggiornaIndirizzi();
    }

    public updateStore() {

        this.store.dispatch(new IndirizziSelezionatiOffFormActions({
            selectedNode: this.selectedNode1,
            selectedSede: this.selectedSede,
            coordinateClicked: this.saveCoordinateClicked,

        }));
    }


    updateEtichette(event) {
        this.infoSediSelected = event;
        this.aggiornaIndirizzi();
    }

    aggiornaFiltraSedi(event: FiltraSedi) {
        this.filtraSedi = event;
        this.aggiornaIndirizzi();

    }

    private aggiornaIndirizzi() {
        if (this.indirizziSelezionati && this.indirizziSelezionati.length > 0) {
            const codici = this.indirizziSelezionati.map((item) => {
                return item.codice;
            });


            this.sediCiclo2.getByIndirizziDiStudio(codici, this.annoScolasticoService.getAnnoScolastico().id)
                .subscribe((list) => {
                        this.sediConIndirizzo = list;
                        this.popolaReteScolasticaCiclo2();
                        this.showRisultati = true;
                        this.selectedRow.push(this.selectedSede);
                    }
                );


            this.mappaPuglia.map.removeLayer('percorrenzeLayer');

            const disponibili = [
                'IP11', 'IP13', 'IP14', 'IP15', 'IP16', 'IP17', 'IP18', 'IP19', 'IP20', 'IP21', 'IT01', 'IT04', 'IT05',
                'IT09', 'IT10', 'IT13', 'IT15', 'IT16', 'IT19', 'IT21', 'IT24', 'LA', 'LC', 'LL', 'LM', 'LS', 'LSU'
            ];


            const diff = _.intersectionWith(codici, disponibili, _.isEqual);

            if (diff.length === 1) {
                // layer percorrenze
                this.percorrenzeLayer = new VectorLayer({
                    source: new Vector({
                        url: '/assets/maps/percorrenze/' + diff[0] + '.json',
                        format: new GeoJSON(),
                    }),
                    zIndex: 10,
                    style: (feature) => this.getStylePercorrenze(feature)
                });
                this.percorrenzeLayer.set('id', 'percorrenzeLayer');
                this.mappaPuglia.map.addLayer(this.percorrenzeLayer);
            } else {
                if (this.percorrenzeLayer != null) {
                    this.mappaPuglia.map.removeLayer(this.percorrenzeLayer);
                }
            }
        } else {
            this.showRisultati = false;
            this.popupSedeSelezionata = null;
            const layerVector: VectorLayer[] = [];
            this.mappaPuglia.map.getLayers().forEach(item => {

                if (item.get('id') === 'percorrenzeLayer') {
                    layerVector.push(item);

                }
            });


            if (this.reteScolastica2Layer != null) {
                this.mappaPuglia.map.removeLayer(this.reteScolastica2Layer);
                this.mappaPuglia.map.removeLayer(this.linkLayer2);
            }
            layerVector.forEach(item => this.mappaPuglia.map.removeLayer(item));

        }

    }


    indirizzoAndSedeSelected(event: any) {

        this.selectedSede = event.data;
        this.saveCoordinateClicked = this.selectedSede.coordinateSuMappa;

    }


    ngOnDestroy(): void {
        this.updateStore();
    }


    private popolaReteScolasticaCiclo2() {

        let i = 0;
        if (this.reteScolastica2Layer != null) {
            this.mappaPuglia.map.removeLayer(this.reteScolastica2Layer);
            this.mappaPuglia.map.removeLayer(this.linkLayer2);
            this.pointsUtility.resetPunti();
        }

        const features2 = [];

        this.sediConIndirizzo.forEach((sede) => {
            if (sede.coordinate) {
                const coordinates = this.pointsUtility.assicuraPuntoUnivoco(sede.coordinate.coordinates);
                Sede.setCoordinateSuMappa(sede, coordinates);
                const p = new Point(sede.coordinateSuMappa);
                const feature2 = new Feature({geometry: p, sede: sede});
                feature2.setId(sede.codice);
                feature2.setStyle((feature, resolution) => {
                    const displayText = resolution <= this.mappaPuglia.view.getResolutionForZoom(10);
                    let label: String = '';
                    if (displayText) {
                        const alunni = (sede.alunni && sede.alunni.totaleAlunni) ? sede.alunni.totaleAlunni : '';

                        switch (this.infoSediSelected) {
                            case InfoSedi.TUTTI:
                                label = sede.denominazione + '\n' + sede.tipologiaScuola + alunni + '/' + 'Alunni';
                                break;
                            case InfoSedi.NOMI:
                                label = sede.denominazione;
                                break;
                            case InfoSedi.OD_OF:
                                label = sede.tipologiaScuola + alunni + '/' + 'Alunni';
                                break;
                            case InfoSedi.DISATTIVA:
                                label = '';
                                break;

                        }
                    }
                    let visualizza = true;


                    if (sede.indirizziDiStudio.length > 0) {

                        const performFiltering = this.filtraSedi.filtraSediCiclo2.showOnlyCiclo2LI
                            || this.filtraSedi.filtraSediCiclo2.showOnlyCiclo2IP || this.filtraSedi.filtraSediCiclo2.showOnlyCiclo2IT;
                        visualizza = !this.filtraSedi.filtraSediCiclo2.showOnlySediCiclo2 || (!performFiltering ||
                            (this.filtraSedi.filtraSediCiclo2.showOnlyCiclo2IP && sede.indirizziDiStudio[0].codice.substr(0, 2) === 'IP') ||
                            (this.filtraSedi.filtraSediCiclo2.showOnlyCiclo2IT && sede.indirizziDiStudio[0].codice.substr(0, 2) === 'IT') ||
                            (this.filtraSedi.filtraSediCiclo2.showOnlyCiclo2LI && sede.indirizziDiStudio[0].codice.substr(0, 2) === 'LI'));

                    } else {
                        console.error('Punto di erogazione senza indirizzi ', sede);
                    }


                    return this.buildMapsService.getStyleSede(feature, sede, displayText, label, visualizza);
                });
                features2[i++] = feature2;
            }
        });

        this.reteScolastica2Layer = new VectorLayer({
            wrapX: false,
            source: new Vector({
                features: features2,
            }),
            zIndex: 12,
        });
        this.mappaPuglia.map.addLayer(this.reteScolastica2Layer);
        console.log('rete caricata (' + i + ') sedi');
    }

    public goIs(codice: String) {
        this.router.navigate(['pages/analisi'], {
            queryParams: {
                ist: codice,
                anno: this.annoScolasticoService.getAnnoScolastico().id
            }
        });
    }

    private getStylePercorrenze(feature: any) {
        const count = feature.values_.Join_Count;
        const key = '' + feature.id + count;
        const cached = this.cachedStyles[key];
        if (cached) {
            return cached;
        }
        const alpha: Number = 0.8;
        const color: String = OffertaFormativaCiclo2Component.getLayerColor(count);
        if (color == null) {
            console.warn('color not found for feature count ' + count);
            return null;
        }
        const fill = new Fill({
            color: color
        });
        this.cachedStyles[key] = new Style({
            fill: fill,
            stroke: null,
        });
        return this.cachedStyles[key];
    }

    annullaSelezione() {
        this.indirizziSelezionati = [];
        this.selectedNode1 = [];
        this.tabellaIndirizzi.selection = [];
        this.aggiornaIndirizzi();
    }
}
