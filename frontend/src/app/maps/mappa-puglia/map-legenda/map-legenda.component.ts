/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {distanzaMinimaMeters} from '../../../shared/helper/punti-univoci';
import {AreaLegendField, LegendField, LineLegendField, PointLegendField} from './model';

@Component({
    selector: 'app-map-legenda',
    templateUrl: './map-legenda.component.html',
    styleUrls: ['./map-legenda.component.scss']
})
export class MapLegendaComponent implements OnInit {


    readonly distanzaPE = distanzaMinimaMeters;

    @Input()
    showLegend: boolean;

    @Input() fields: LegendField[] = [];

    @Output()
    updateShowLegend: EventEmitter<boolean>;


    constructor() {
        this.updateShowLegend = new EventEmitter<boolean>();
    }

    public toggleLegenda() {
        this.showLegend = !this.showLegend;
        this.updateShowLegend.emit(this.showLegend);
    }

    ngOnInit() {

    }


    isArea(field: LegendField) {
        return field instanceof AreaLegendField;
    }

    isPunto(field: LegendField) {
        return field instanceof PointLegendField;
    }

    isLine(field: LegendField) {
        return field instanceof LineLegendField;
    }
}
