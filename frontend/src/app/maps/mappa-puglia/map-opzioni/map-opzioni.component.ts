/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {OpzioniMap} from '../../../shared/models/opzioni-map/OpzioniMap';

@Component({
    selector: 'app-map-opzioni',
    templateUrl: './map-opzioni.component.html',
    styleUrls: ['./map-opzioni.component.css']
})
export class MapOpzioniComponent implements OnInit {


    readonly title = 'Opzioni';
    @Input() disableLeggibilita: boolean = false;
    @Input() disableSpostamenti: boolean = false;
    @Input() disableEdifici: boolean = false;
    @Input() disableLegenda: boolean = false;

    @Input()
    opzioniMap: OpzioniMap;

    @Output()
    updateOpzioniMap: EventEmitter<OpzioniMap>;

    constructor() {
        this.updateOpzioniMap = new EventEmitter<OpzioniMap>();
    }

    ngOnInit() {
    }


    toggleEdificiInutilizzati() {
        this.opzioniMap.mostraEdificiInutilizzati = !this.opzioniMap.mostraEdificiInutilizzati;
        this.updateOpzioniMap.emit(this.opzioniMap);
    }

    toggleLegenda() {
        this.opzioniMap.showLegend = !this.opzioniMap.showLegend;
        this.updateOpzioniMap.emit(this.opzioniMap);
    }

    toggleSpostamenti() {
        this.opzioniMap.showSpostamenti = !this.opzioniMap.showSpostamenti;
        this.updateOpzioniMap.emit(this.opzioniMap);
    }

    toggleFiltroLeggibilita() {
        this.opzioniMap.applicaLeggibilita = !this.opzioniMap.applicaLeggibilita;
        this.updateOpzioniMap.emit(this.opzioniMap);
    }

}
