/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FiltraSedi} from '../../../shared/models/sedi/FiltraSedi';

@Component({
    selector: 'app-map-filtro-sedi',
    templateUrl: './map-filtro-sedi.component.html',
    styleUrls: ['./map-filtro-sedi.component.scss']
})
export class MapFiltroSediComponent implements OnInit {

    @Input() disableAllFilter = false;

    @Input() disableCliclo1Filter = false;

    @Input() disableCliclo2Filter = false;

    @Input() filtraSedi: FiltraSedi;

    @Output()
    updateFiltraSedi: EventEmitter<FiltraSedi>;


    constructor() {
        this.updateFiltraSedi = new EventEmitter<FiltraSedi>();
    }

    ngOnInit() {
    }

    setFilterSediOff() {
        this.filtraSedi.filtraSediCiclo1.showOnlySediCiclo1 = false;
        this.filtraSedi.filtraSediCiclo2.showOnlySediCiclo2 = false;
        this.filtraSedi.filtraSediCiclo1.showOnlyCiclo1EE = false;
        this.filtraSedi.filtraSediCiclo1.showOnlyCiclo1MM = false;
        this.filtraSedi.filtraSediCiclo1.showOnlyCiclo1AA = false;
        this.filtraSedi.filtraSediCiclo2.showOnlyCiclo2LI = false;
        this.filtraSedi.filtraSediCiclo2.showOnlyCiclo2IP = false;
        this.filtraSedi.filtraSediCiclo2.showOnlyCiclo2IT = false;
        this.updateFiltraSedi.emit(this.filtraSedi);
    }

    public setFilterSediCiclo1GradoOff() {

        this.filtraSedi.filtraSediCiclo1.showOnlyCiclo1AA = false;
        this.filtraSedi.filtraSediCiclo1.showOnlyCiclo1EE = false;
        this.filtraSedi.filtraSediCiclo1.showOnlyCiclo1MM = false;
        this.filtraSedi.filtraSediCiclo2.showOnlyCiclo2LI = false;
        this.filtraSedi.filtraSediCiclo2.showOnlyCiclo2IP = false;
        this.filtraSedi.filtraSediCiclo2.showOnlyCiclo2IT = false;
        this.setFilterSediCiclo1();

    }


    public setFilterSediCiclo1() {
        this.filtraSedi.filtraSediCiclo1.showOnlySediCiclo1 = true;
        this.filtraSedi.filtraSediCiclo2.showOnlySediCiclo2 = false;
        this.updateFiltraSedi.emit(this.filtraSedi);
    }


    public setFilterCiclo1AA() {
        this.filtraSedi.filtraSediCiclo1.showOnlyCiclo1EE = false;
        this.filtraSedi.filtraSediCiclo1.showOnlyCiclo1MM = false;
        this.filtraSedi.filtraSediCiclo1.showOnlyCiclo1AA = !this.filtraSedi.filtraSediCiclo1.showOnlyCiclo1AA;
        this.setFilterSediCiclo1();
    }

    public setFilterCiclo1EE() {
        this.filtraSedi.filtraSediCiclo1.showOnlyCiclo1AA = false;
        this.filtraSedi.filtraSediCiclo1.showOnlyCiclo1MM = false;
        this.filtraSedi.filtraSediCiclo1.showOnlyCiclo1EE = !this.filtraSedi.filtraSediCiclo1.showOnlyCiclo1EE;
        this.setFilterSediCiclo1();
    }

    public setFilterCiclo1MM() {
        this.filtraSedi.filtraSediCiclo1.showOnlyCiclo1AA = false;
        this.filtraSedi.filtraSediCiclo1.showOnlyCiclo1EE = false;
        this.filtraSedi.filtraSediCiclo1.showOnlyCiclo1MM = !this.filtraSedi.filtraSediCiclo1.showOnlyCiclo1MM;
        this.setFilterSediCiclo1();
    }


    public setFilterSediCiclo2IndirizziOff() {
        this.filtraSedi.filtraSediCiclo1.showOnlySediCiclo1 = false;
        this.filtraSedi.filtraSediCiclo1.showOnlyCiclo1AA = false;
        this.filtraSedi.filtraSediCiclo1.showOnlyCiclo1EE = false;
        this.filtraSedi.filtraSediCiclo1.showOnlyCiclo1MM = false;


        this.filtraSedi.filtraSediCiclo2.showOnlyCiclo2LI = false;
        this.filtraSedi.filtraSediCiclo2.showOnlyCiclo2IP = false;
        this.filtraSedi.filtraSediCiclo2.showOnlyCiclo2IT = false;
        this.filtraSedi.filtraSediCiclo2.showOnlySediCiclo2 = true;
        this.updateFiltraSedi.emit(this.filtraSedi);

    }


    public setFilterSediCiclo2() {
        this.filtraSedi.filtraSediCiclo1.showOnlySediCiclo1 = false;
        this.filtraSedi.filtraSediCiclo2.showOnlySediCiclo2 = true;
        this.updateFiltraSedi.emit(this.filtraSedi);
    }


    public setFilterCiclo2LI() {
        this.filtraSedi.filtraSediCiclo2.showOnlyCiclo2LI = !this.filtraSedi.filtraSediCiclo2.showOnlyCiclo2LI;
        this.filtraSedi.filtraSediCiclo2.showOnlyCiclo2IP = false;
        this.filtraSedi.filtraSediCiclo2.showOnlyCiclo2IT = false;
        this.setFilterSediCiclo2();

    }

    public setFilterCiclo2IT() {

        this.filtraSedi.filtraSediCiclo2.showOnlyCiclo2LI = false;
        this.filtraSedi.filtraSediCiclo2.showOnlyCiclo2IP = false;
        this.filtraSedi.filtraSediCiclo2.showOnlyCiclo2IT = !this.filtraSedi.filtraSediCiclo2.showOnlyCiclo2IT;

        this.setFilterSediCiclo2();
    }

    public setFilterCiclo2IP() {
        this.filtraSedi.filtraSediCiclo2.showOnlyCiclo2LI = false;
        this.filtraSedi.filtraSediCiclo2.showOnlyCiclo2IP = !this.filtraSedi.filtraSediCiclo2.showOnlyCiclo2IP;
        this.filtraSedi.filtraSediCiclo2.showOnlyCiclo2IT = false;
        this.setFilterSediCiclo2();

    }


}
