/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {ActionReducerMap, MetaReducer,} from '@ngrx/store';
import Center, {Extent} from 'ol';
import {environment} from '../../environments/environment';
import {MapActionTypes} from '../maps/assetto-is/map-action.actions';
import {InfoSedi} from '../maps/mappa-puglia/Utils';
import {fromLonLat} from 'ol/proj.js';
import {OffertaFormativaState, offFormativaReducer} from '../maps/offerta-formativa-ciclo2/offerta-formativa.reducer';


// carica i dati dal local storage per inizializzare il sistema
// dopo reload nuova tab

const dati = JSON.parse(localStorage.getItem(('map-state')));

export interface OpzioniState {
    mostraLegenda: boolean;
    mostraSpostamenti: boolean;
    filtroLeggibilita: boolean;
    mostraEdificiInutilizzati: boolean;
}

export interface FilterCicloState {

    showOnlySediCiclo1: boolean;
    showOnlySediCiclo2: boolean;
    showOnlyCiclo1AA: boolean;
    showOnlyCiclo1EE: boolean;
    showOnlyCiclo1MM: boolean;
    showOnlyCiclo2LI: boolean;
    showOnlyCiclo2IP: boolean;
    showOnlyCiclo2IT: boolean;


}


export interface DimensionateState {
    showOnlySovraDimensionate: boolean;
    showOnlySottoDimensionate: boolean;


}

const initialFilterCicloState: FilterCicloState = {


    showOnlySediCiclo1: false,
    showOnlySediCiclo2: false,
    showOnlyCiclo1AA: false,
    showOnlyCiclo1EE: false,
    showOnlyCiclo1MM: false,
    showOnlyCiclo2LI: false,
    showOnlyCiclo2IP: false,
    showOnlyCiclo2IT: false


};

const initialOpzioniState: OpzioniState = {


    mostraLegenda: true,
    mostraSpostamenti: true,
    filtroLeggibilita: true,
    mostraEdificiInutilizzati: false,
};

const initialDimensionateState: DimensionateState = {

    showOnlySottoDimensionate: false,
    showOnlySovraDimensionate: false

};


interface MappaDinamicaState {
    opzioni: OpzioniState;
    filterCiclo: FilterCicloState;
    dimensionate: DimensionateState;
    infoSedi: string;
    lastZoom: number;
    lastExtent: Extent;
    center: Center;


}

const initialMapState: MappaDinamicaState = {
    opzioni: initialOpzioniState,
    filterCiclo: initialFilterCicloState,
    dimensionate: initialDimensionateState,
    infoSedi: InfoSedi.TUTTI,
    lastZoom: 8,
    lastExtent: null,
    center: fromLonLat([16.866342, 41.118151]),


};

export function mapReducer(state: MappaDinamicaState = initialMapState, action): MappaDinamicaState {
    switch (action.type) {
        case MapActionTypes.ModificaFiltri:
            return {
                opzioni: action.payload.opzioni,
                filterCiclo: action.payload.filterCiclo,
                infoSedi: action.payload.infoSedi,
                dimensionate: action.payload.dimensionate,
                lastZoom: action.payload.lastZoom,
                lastExtent: action.payload.lastExtent,
                center: action.payload.center

            };

        default:
            return state;

    }

}

export interface AppState {

    mapDinamicaState: MappaDinamicaState;
    offFormativaState: OffertaFormativaState,

}

export const reducers: ActionReducerMap<AppState> = {
    mapDinamicaState: mapReducer,
    offFormativaState: offFormativaReducer

};


export const metaReducers: MetaReducer<AppState>[] = !environment.production ? [] : [];
