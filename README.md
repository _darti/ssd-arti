# SSD - Sistema per il dimensionamento scolastico

SSD è un sistema geocartografico di supporto al dimensionamento scolastico, realizzato dalla Regione Puglia in sinergia 
con l’Osservatorio Regionale dei Sistemi di Istruzione e Formazione in Puglia di ARTI Puglia.

![Assetto istituzioni scolastiche](docs/img/assettoIstituzioniScolastiche.jpg)

Consiste in un portale web di tipo webGIS e consente a cittadini, scuole, Comuni, Province, Uffici Scolastici Provinciali
 e Regionali e, in generale, agli attori coinvolti la visualizzazione dell’assetto e dell'offerta formativa delle 
 Istituzioni Scolastiche regionali, con la caratterizzazione dei gradi di istruzione e degli indirizzi di studio, delle 
 istituzioni scolastiche sovra e sottodimensionate, dei dati demografici dei comuni e del trend delle iscrizioni 
 nell’ultimo triennio.
 
![Offerta formativa del II ciclo](docs/img/offertaFormativaIIciclo.jpg)
 
![CPIA](docs/img/cpia.jpg)
  
A breve il sistema consentirà la lettura dei dati inerenti gli organici delle Istituzioni Scolastiche, la previsione 
 della popolazione scolastica, la provenienza geografica degli iscritti e i bacini di utenza, la “storia” delle modifiche 
 sull’assetto nell’ultimo triennio, i settori economico-produttivi trainanti per ciascuna Provincia, ecc.

È disponibile per la fruizione all'indirizzo https://ssd.regione.puglia.it/

## Strutura del repository
Il sistema consiste in una web application strutturato in due macro aeree, il backend e il frontend.

### Backend
Il backend è la componente software nella quale risiedono le logiche di business, esso si occupa di gestire 
 l’importazione dei dati, la loro manipolazione e l’esposizione degli stessi verso il frontend.
 La comunicazione fra backend e frontend avviene tramite API RESTfull, in cui i dati vengono serializzati nel formato  JSON.

I componenti principali che compongono il backend sono:
 
 *  Console Application
 *  Web Application
 *  DBMS PostgreSQL 

Sia la web application che la console application sono state implementate in  PHP utilizzando il framework  Laravel 5.8.
 Per gestire le dipendenze di PHP viene usato composer.
 Il DBMS PostgreSQL viene usato per la persistenza dei dati, mentre per poter operare con dati georeferenziati, 
 è stata scelta l’estensione PostGIS.

### Frontend
Il modulo di front-end è stato sviluppato usando il framework Angular, versione 7.
 Di conseguenza il client del progetto è una single page application che interagisce con il back-end mediante l’utilizzo 
 di Api Rest, questo facilita la gestione e la separazione di operazioni di tipo CRUD e l’elaborazione avanzata di dati 
 complessi (es. dati cartografici) 
 Il front-end, come per il back-end, segue l'architettura MVC, altamente diffusa per la sua peculiarità di separare le 
 responsabilità dei vari componenti che sistematicamente gestiscono la sequenza di operazioni comunemente eseguite dal 
 compilatore installato nei browser.

## Prerequisiti
Per l'installazione del sistema è necessario soddisfare i seguenti prerequisiti:

 *  PHP 7.2
 *  Composer
 *  PostgreSQL 9
 *  PostGIS
 *  Webserver (consigliato Nginx)

Per compilare il frontend è necessario inoltre:

 *  NodeJS
 *  Npm

## Installazione 

### Setup del DBMS
Dopo essersi collegati tramite console al database PostgreSQL che si vuole configurare, è necessario eseguire lo script 
seguente:

```postgresql
create database :'db_name';
\c :'db_name';
CREATE SCHEMA ssd;
CREATE EXTENSION postgis SCHEMA ssd;
create user api with encrypted password :'db_password';
grant all privileges on database :'db_name' to api;
grant all privileges on schema ssd to api;
``` 
Nota: nome e password del database sono stati dichiarati come variabili. Scegliere ed annotare dei valori appropriati.

### Configurazione del backend

```shell
cd backend
composer install
cp .env.example .env
php artisan key:generate
```

A questo punto è neecessario modificare il file .env. Inserire i compi contrassegnati con `TODO`. Dopo di che è necessario
eseguire la migrazione del database.

```shell
php artisan migrate
php artisan route:cache
```

Per importare i dati, fare riferimento al  [readme](backend/README.md) presente nella cartella backend. 

A questo punto è possibile verificare il corretto funzionamento avviando un web server di deployment eseguendo il comando

```shell
php artisan serve
```
e visitando con un browser l'indirizzo ```http://127.0.0.1:8000/api/annoScolastico```

### Configurazione del frontend

È necessarop installare le dipendenze
```shell
cd frontend
npm install
```

A questo punto è possibile verificare il corretto funzionamento avviando un web server di deployment eseguendo il comando

```shell
ng serve
```

e visitando con un browser l'indirizzo ```http://localhost:4200/```

## Copyright 
Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.

Il codice è rilasciato con licenza GNU-AGPL 3.0, mentre la documentazione con licenza Creative Commons CC-BY 4.0
